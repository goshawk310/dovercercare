@extends('user.layout')

@section('custom-styles')
{{ HTML::style('/assets/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css') }}
{{ HTML::style('/assets/metronic/global/plugins/slider-revolution-slider/rs-plugin/css/settings.css') }}
{{ HTML::style('/assets/metronic/frontend/pages/css/style-revolution-slider.css') }}
<style>
    #owl-demo .item{
      margin: 7px;
    }
</style>
@stop

@section('main')
<div class="page-slider margin-bottom-40">
      <div class="fullwidthbanner-container revolution-slider">
        <div class="fullwidthabnner">
          <ul id="revolutionul">
            <!-- THE NEW SLIDE -->
            <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="{{ HTTP_COMPANY_PATH }}carousel-babysitter4.jpg">
              <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
              <img src="{{ HTTP_COMPANY_PATH }}carousel-babysitter4.jpg" alt="">
              
              <div class="caption lft slide_title_white slide_item_left"
                data-x="30"
                data-y="90"
                data-speed="400"
                data-start="1500"
                data-easing="easeOutExpo">
                Treat Yourself To <br><span class="slide_title_white_bold">Some Time</span>
              </div>
              <div class="caption lft slide_subtitle_white slide_item_left"
                data-x="87"
                data-y="245"
                data-speed="400"
                data-start="2000"
                data-easing="easeOutExpo">
                Browse the profiles of great service providers.
              </div>
              <a class="caption lft btn dark slide_btn slide_item_left" href="#"
                data-x="187"
                data-y="315"
                data-speed="400"
                data-start="3000"
                data-easing="easeOutExpo">
                Book Now!
              </a>              
            </li>    

            <!-- THE FIRST SLIDE -->
            <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="{{ HTTP_COMPANY_PATH }}carousel-yoga.jpg">
              <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
              <img src="{{ HTTP_COMPANY_PATH }}carousel-yoga.jpg" alt="">
                            
              <div class="caption lft slide_title slide_item_left"
                data-x="30"
                data-y="105"
                data-speed="400"
                data-start="1500"
                data-easing="easeOutExpo">
                Need a service? 
              </div>
              <div class="caption lft slide_subtitle slide_item_left"
                data-x="30"
                data-y="180"
                data-speed="400"
                data-start="2000"
                data-easing="easeOutExpo">
                This is what you were looking for
              </div>
              <div class="caption lft slide_desc slide_item_left"
                data-x="30"
                data-y="220"
                data-speed="400"
                data-start="2500"
                data-easing="easeOutExpo">
                Lorem ipsum dolor sit amet, dolore eiusmod<br> quis tempor incididunt. Sed unde omnis iste.
              </div>
              <a class="caption lft btn green slide_btn slide_item_left" href="#"
                data-x="30"
                data-y="290"
                data-speed="400"
                data-start="3000"
                data-easing="easeOutExpo">
                Book Now!
              </a>              
            </li>

            <!-- THE SECOND SLIDE -->
            <li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-delay="9400" data-thumb="{{ HTTP_COMPANY_PATH }}carousel-massage.jpg">                        
              <img src="{{ HTTP_COMPANY_PATH }}carousel-massage.jpg" alt="">
              <div class="caption lfl slide_title slide_item_left"
                data-x="30"
                data-y="125"
                data-speed="400"
                data-start="3500"
                data-easing="easeOutExpo">
                Powerfull &amp; Clean
              </div>
              <div class="caption lfl slide_subtitle slide_item_left"
                data-x="30"
                data-y="200"
                data-speed="400"
                data-start="4000"
                data-easing="easeOutExpo">
                Direct Professional &amp; Service
              </div>
              <div class="caption lfl slide_desc slide_item_left"
                data-x="30"
                data-y="245"
                data-speed="400"
                data-start="4500"
                data-easing="easeOutExpo">
                Lorem ipsum dolor sit amet, consectetuer elit sed diam<br> nonummy amet euismod dolore.
              </div>              
            </li>
                </ul>
                <div class="tp-bannertimer tp-bottom"></div>
            </div>
        </div>
    </div>
    <!-- END SLIDER -->

    <div class="container">
        <div class="row padding-top-normal padding-bottom-normal">
            <div class="col-sm-12 text-center"><h2 class="color-default">Featured Professional</h2></div>
        </div>
        <div class="row">
            @foreach ($companies as $key => $company)
            <div class="col-sm-2">
                <div style="position: relative;" class="thumbnail">
                    <?php 
                        $tooltip = "<h4>".$company->name."</h4>";
                        $tooltip.= "<p><b>Keywords : </b>";
                        $keywords = explode(",", $company->keyword);
                        foreach ($keywords as $subKey => $value) {
                            if ($subKey != 0)
                                $tooltip.=", ";
                            $tooltip.= $value;
                        }
                        $tooltip.= "</p>";
                        $tooltip.= "<p><b>Email : </b>".$company->email."</p>";
                        $tooltip.= "<p><b>Phone : </b>".$company->phone."</p>";                        
                        $tooltip.= "<p><b>VAT ID : </b>".$company->vat_id."</p>";
                        $tooltip.= "<p><b>Description : </b>".substr($company->description, 0, 300)."</p>";
                    ?>
                    <a href="{{ URL::route('company.detail', $company->slug) }}">
                        <div style="height: 145px; width: 100%; background: url({{ HTTP_COMPANY_PATH.$company->photo }}); background-size: cover;" data-toggle="tooltip" data-placement="{{ $key % 6 >=3 ? 'left' : 'right' }}" data-html="true" data-title="{{ $tooltip }}"></div>
                    </a>
                    <div class="featured-company-offer">
                    @if (count($company->offers) > 0)
                        @foreach ($company->offers as $offer)
                            @if (!$offer->is_review)
                            <div class="color-default store-offers"><b>{{ $offer->name." : " }}</b>{{ $offer->price."&euro;" }}</div>
                            @endif
                        @endforeach                    
                    @endif
                    </div>
                    <h4 class="color-default margin-top-xs">
                        <a href="{{ URL::route('company.detail', $company->slug) }}" data-toggle="tooltip" data-placement="{{ $key % 4 == 3 ? 'left' : 'right' }}" data-html="true" data-title="{{ $tooltip }}">
                            {{ $company->name }}
                        </a>
                    </h4>                    
                    <div class="pull-left">
                        <a href="{{ URL::route('company.detail', $company->slug) }}">
                            Detail&nbsp;<i class="fa fa-angle-double-right"></i>
                        </a>
                    </div>
                    <div class="pull-right">                            
                        <input id="js-number-rating" type="number" class="rating" min=0 max=5 step=1 data-show-clear=false data-show-caption=false data-size='xs' value="{{ $company->getRatingScore() }}" readonly=true>
                    </div>
                    <div class="clearfix"></div>
                    
                </div>
            </div>
            @endforeach
        </div>
     </div>
     <div style="background:#eee;">
	     <div class="container">
	     	<div class="row padding-top-normal padding-bottom-normal">
	            <div class="col-sm-12 text-center"><h2 class="color-default">Featured Service</h2></div>        
	        </div>
	     </div>
     </div>
     <div data-vide-bg="mp4: {{ HTTP_COMPANY_PATH }}shv_sunrise.mp4,"
	  data-vide-options="posterType: jpg, loop: true, auto-play:true, muted: false, position: 0% 0%"style="padding:70px 0;">
     <div class="container">        
        <div class="row">
            @foreach ($stores as $key => $store)
            <div class="col-sm-3">
                <div style="position: relative;background-color:rgba(255,255,255,1.0)" class="thumbnail">
                    <?php 
                        $tooltip = "<h4>".$store->name."</h4>";
                        $tooltip.= "<p><b>Keywords : </b>";
                        $keywords = explode(",", $store->keyword);
                        foreach ($keywords as $subKey => $value) {
                            if ($subKey != 0)
                                $tooltip.=", ";
                            $tooltip.= $value;
                        }
                        $tooltip.= "</p>";
                        $tooltip.= "<p><b>Email : </b>".$store->company->email."</p>";
                        $tooltip.= "<p><b>Phone : </b>".$store->company->phone."</p>";
                        $tooltip.= "<p><b>VAT ID : </b>".$store->company->vat_id."</p>";                       
                        $tooltip.= "<p><b>Description : </b>".substr($store->description, 0, 300)."</p>";
                    ?>
                    <a href="{{ URL::route('store.detail', $store->slug) }}">
                        <div style="height: 145px; width: 100%; background: url({{ HTTP_STORE_PATH.$store->photo }}); background-size: cover;" data-toggle="tooltip" data-placement="{{ $key % 4 == 3 ? 'left' : 'right' }}" data-html="true" data-title="{{ $tooltip }}"></div>
                    </a>
                    <div class="featured-company-offer">
                    @if (count($store->company->offers) > 0)
                        @foreach ($store->company->offers as $offer)
                            @if (!$offer->is_review)
                            <div class="color-default store-offers"><b>{{ $offer->name." : " }}</b>{{ $offer->price."&euro;" }}</div>
                            @endif
                        @endforeach                    
                    @endif
                    </div>
                    <h4 class="color-default margin-top-xs">
                        <a href="{{ URL::route('store.detail', $store->slug) }}" data-toggle="tooltip" data-placement="{{ $key % 4 == 3 ? 'left' : 'right' }}" data-html="true" data-title="{{ $tooltip }}">
                            {{ $store->name }}
                        </a>
                    </h4>                    
                    <div class="pull-left">
                        <a href="{{ URL::route('store.detail', $store->slug) }}">
                            Detail&nbsp;<i class="fa fa-angle-double-right"></i>
                        </a>
                    </div>
                    <div class="pull-right">                            
                        <input id="js-number-rating" type="number" class="rating" min=0 max=5 step=1 data-show-clear=false data-show-caption=false data-size='xs' value="{{ $store->getRatingScore() }}" readonly=true>
                    </div>
                    <div class="clearfix"></div>
                    
                </div>
            </div>
            @endforeach
        </div>
     </div>
     </div>
     <div class="container">
        <div class="row padding-top-normal padding-bottom-normal">
            <div class="col-sm-12 text-center"><h2 class="color-default">Recent Reviews</h2></div>        
        </div>
        <div class="row padding-bottom-normal">
            <div class="col-sm-12">
                <div id="owl-demo" class="owl-carousel owl-theme">
                    @foreach ($feedbacks as $feedback)
                    <div class="item">
                        <div class="testimonials-v1">
                            <blockquote><p>{{ $feedback->description }}</p></blockquote>
                            <table>
                                @foreach ($feedback->ratings as $rating)
                                <tr>
                                    <td class="text-right"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $rating->type->name }}&nbsp;:&nbsp;</b></td>
                                    <td>
                                        <input id="js-number-rating" type="number" class="rating" min=0 max=5 step=1 data-show-clear=false data-show-caption=false data-size='xs' value="{{ $rating->answer }}" readonly=true>                                
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                            <div class="carousel-info">
                                <img class="pull-left" src="{{ HTTP_USER_PATH.$feedback->user->photo }}" style="height: 60px; width: 60px;">
                                <div class="pull-left">
                                    <span class="testimonials-name" style="margin-top: 10px;">{{ $feedback->user->name }}</span>
                                    <span class="testimonials-post"><i class="fa fa-clock-o"></i>&nbsp;{{ date(DATE_FORMAT, strtotime($feedback->created_at)) }}</span>
                                </div>
                            </div>                    
                        </div>
                    </div>
                    @endforeach
                </div>    
            </div>        
        </div>
    </div>
@stop

@section('custom-scripts')
    {{ HTML::script('/assets/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js') }}
    {{ HTML::script('/assets/metronic/global/plugins/jquery.vide.js') }}
    {{ HTML::script('/assets/metronic/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.plugins.min.js') }}
    {{ HTML::script('/assets/metronic/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js') }}
    {{ HTML::script('/assets/metronic/frontend/pages/scripts/revo-slider-init.js') }}
    {{ HTML::script('/assets/js/bootstrap-tooltip.js') }}
    @include('js.user.home.index')
    <script>
    $(document).ready(function() {
    	jQuery('.fullwidthabnner').revolution();
        $("#owl-demo").owlCarousel({            
            autoPlay: 3000, //Set AutoPlay to 3 seconds       
            items : 4,
            itemsDesktop : [1199,3],
            itemsDesktopSmall : [979,3]
        });        
    });    
    </script>      
@stop

@stop
