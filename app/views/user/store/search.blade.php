@extends('user.layout')

@section('main')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="pull-right margin-top-xs">
                <button class="btn {{ (Session::get('dt') == 'grid') ? 'btn-primary' : 'btn-default' }}" id="js-btn-display" data-display="grid"><i class="fa fa-th"></i></button>
                <button class="btn {{ (Session::get('dt') == 'list') ? 'btn-primary' : 'btn-default' }}" id="js-btn-display" data-display="list"><i class="fa fa-th-list"></i></button>
            </div>
        </div>
    </div>
    @if (Session::get('dt') == 'grid')
    <div class="row margin-top-xs margin-bottom-sm">
        @if (count($stores) > 0)
            <div class="col-sm-12 pull-right">{{ $stores->appends(Request::input())->links() }}</div>
            <div class="clearfix"></div>    
            @foreach ($stores as $key => $store)
                <div class="col-sm-3">
                    <div class="thumbnail company-list">
                        <a href="{{ URL::route('store.detail', $store->slug) }}">
                            <?php 
                                $tooltip = "<h4>".$store->name."</h4>";
                                $tooltip.= "<p><b>Keywords : </b>";
                                $keywords = explode(",", $store->keyword);
                                foreach ($keywords as $subKey => $value) {
                                    if ($subKey != 0)
                                        $tooltip.=", ";
                                    $tooltip.= $value;
                                }
                                $tooltip.= "</p>";
                                $tooltip.= "<p><b>Email : </b>".$store->company->email."</p>";
                                $tooltip.= "<p><b>Phone : </b>".$store->phone."</p>";
                                $tooltip.= "<p><b>VAT ID : </b>".$store->company->vat_id."</p>";                                
                                $tooltip.= "<p><b>Description : </b>".substr($store->description, 0, 300)."</p>";
                            ?>
                            <div style="height: 145px; width: 100%; background: url({{ HTTP_STORE_PATH.$store->photo }}); background-size: cover;" data-toggle="tooltip" data-placement="{{ $key % 4 == 3 ? 'left' : 'right' }}" data-html="true" data-title="{{ $tooltip }}"></div>
                        </a>
                        <h4 class="color-default margin-top-xs">
                            <a href="{{ URL::route('store.detail', $store->slug) }}" data-toggle="tooltip" data-placement="{{ $key % 4 == 3 ? 'left' : 'right' }}" data-html="true" data-title="{{ $tooltip }}">
                                {{ $store->name }}
                            </a>
                        </h4>
                        
                        <div class="row">
                            <p class="col-sm-4  text-right color-default"><b>Email : </b></p>
                            <p class="col-sm-7 no-padding-left">{{ $store->company->email }}</p>
                        </div>
                        
                        <div class="row">
                            <p class="col-sm-4 text-right color-default"><b>Tel : </b></p>
                            <p class="col-sm-7 no-padding-left">{{ $store->company->phone }}</p>
                        </div>                       
                        
                        <div class="row">
                            <p class="col-sm-4 text-right color-default"><b>Officies : </b></p>
                            	<div class="col-sm-7 no-padding-left">
                            		<?php $i = 0;?>
		                            @foreach($store->officies as $office)
		                            	<?php $i++; ?>
		                            	<p class="col-sm-12" style="padding-left:0;">{{ $office->office->name }} {{$i == 2 ? ' ...' : ''}}</p>
		                            	<?php if($i == 2) break;?>
		                            @endforeach
	                            </div>
                        </div>
                      
                        <div class="pull-left">
                            @if (Session::has('user_id'))
                            <!-- <button class="btn btn-primary btn-sm" data-id="{{ $store->id }}" id="js-btn-add-cart"><i class="fa fa-plus"></i> Cart</button> -->
                            @endif
                        </div>
                        <div class="pull-right" style="margin-top: -10px;">
                            <input id="js-number-rating" type="number" class="rating" min=0 max=5 step=1 data-show-clear=false data-show-caption=false data-size='xs' value="{{ $store->getRatingScore() }}" readonly=true>
                        </div>
                        <div class="clearfix"></div>
                        
                    </div>
                </div>        
            @endforeach
        @else
            <div class="note note-info">
			    <h4 class="block">There is no search result.</h4>
			</div>
        @endif
    </div>
    @else
    <div class="row margin-top-xs margin-bottom-sm">
        <div class="col-sm-7">
            @if (count($stores) > 0)
                <div class="pull-right">{{ $stores->appends(Request::input())->links() }}</div>
                <div class="clearfix"></div>
                @foreach ($stores as $key => $store)
                <div class="row padding-top-xs padding-bottom-xs border-bottom-gray company-item" data-id="{{ $key }}">
                    <div class="col-md-4 col-sm-4">
                        <a href="{{ URL::route('store.detail', $store->slug) }}">
                            <?php 
                            	$tooltip = "<div class='testimonials-v1'><div class='carousel-info'><img style='width:50px;height:50px;float:left;' src='". HTTP_COMPANY_PATH.$store->company->photo ."'/><h4 style='float:left;margin-left:10px;margin-top:12px;'>".$store->company->name."</h4></div><div style='clear:both;'></div></div>";
                                //$tooltip = "<h4>".$store->name."</h4>";
                                $tooltip.= "<p><b>Keywords : </b>";
                                $keywords = explode(",", $store->keyword);
                                foreach ($keywords as $subKey => $value) {
                                    if ($subKey != 0)
                                        $tooltip.=", ";
                                    $tooltip.= $value;
                                }
                                $tooltip.= "</p>";
                                $tooltip.= "<p><b>Email : </b>".$store->company->email."</p>";
                                $tooltip.= "<p><b>Phone : </b>".$store->phone."</p>";
                                $tooltip.= "<p><b>VAT ID : </b>".$store->company->vat_id."</p>";                                
                                $tooltip.= "<p><b>Description : </b>".substr($store->description, 0, 300)."</p>";
                            ?>
                            <div class="img-rounded" style="height: 135px; width: 100%; background: url({{ HTTP_STORE_PATH.$store->photo }}); background-size: cover;" data-toggle="tooltip" data-placement="right" data-html="true" data-title="{{ $tooltip }}"></div>                            
                        </a>
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <h3 class="pull-left">
                            <a href="{{ URL::route('store.detail', $store->slug) }}" data-toggle="tooltip" data-placement="right" data-html="right" data-title="{{ $tooltip }}">{{ $store->name }}</a>
                            @if (Session::has('user_id'))
                            <!-- <button class="btn btn-link btn-sm" data-id="{{ $store->id }}" id="js-btn-add-cart" title="Add Cart"><i class="fa fa-shopping-cart"></i></button> -->
                            @endif                            
                        </h3>
                        <div class="pull-right">
                            <input id="js-number-rating" type="number" class="rating" min=0 max=5 step=1 data-show-clear=false data-show-caption=false data-size='xs' value="{{ $store->getRatingScore() }}" readonly=true>
                        </div>                        
                        <div class="clearfix"></div>
                         <ul class="blog-info">
                            <li><?php echo substr($store->description, 0, 200);?></li>
                        </ul>
                        <ul class="blog-info">
                        	<li><span class="color-default"><b><i class="fa fa-envelope"></i> {{ $store->company->email }}</b></li>
                            <li><span class="color-default"><b><i class="fa fa-phone"></i> {{ $store->company->phone }}</b></li>
                        </ul>                        
                    </div>
                </div>
                @endforeach
                <div class="pull-right">{{ $stores->appends(Request::input())->links() }}</div>
                <div class="clearfix"></div>
            @else
                <div class="note note-info">
				    <h4 class="block">There is no search result.</h4>
				</div>
            @endif
        </div>
                
        <div class="col-sm-5">
            <div class="clearfix"></div>
            <div id="map-canvas" style="height: 500px; width: 100%; border: 2px solid #E6400C;" class="margin-top-xs margin-bottom-xs"></div>
            <a href="http://inquirymall.com" target="_blank" class="margin-top-xs"><img src="/assets/img/partner1.jpg" class="img-responsive"/></a>
            <a href="http://socialheadhunter.org" target="_blank" class="margin-top-xs"><img src="/assets/img/partner2.jpg" class="img-responsive"/></a>
            <a href="http://klikkaaja.com" target="_blank" class="margin-top-xs"><img src="/assets/img/partner3.jpg" class="img-responsive"/></a>
        </div>
    </div>
    @endif
</div>
 
@stop

@section('custom-scripts')
{{ HTML::script('/assets/js/bootstrap-tooltip.js') }}
<script>
var markers = new Array();
var alternateMarkers=[], markersIcon=[];
$(document).ready(function() {
	
    $("button#js-btn-add-cart").click(function() {
        $.ajax({
            url: "{{ URL::route('async.user.cart.add') }}",
            dataType : "json",
            type : "POST",
            data : { store_id : $(this).attr('data-id') },
            success : function(data){
                bootbox.alert(data.msg);
                window.setTimeout(function(){
                    bootbox.hideAll();
                }, 2000);
            }
        });
    });

    $("button#js-btn-display").click(function() {
        $("input[name='dt']").val($(this).attr('data-display'));
        $("button#js-btn-search").click();
    });

    $("div.company-item").find("*").mouseover(function(event) {
        event.preventDefault();
    });

    $("div.company-item").hover(function() {
        var ind = $(this).attr("data-id");
        Object.size = function(obj) {
            var size = 0, key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
        };
        
        for(var i = 0; i < Object.size(marker[ind])-1; i++){
        	var lat = marker[ind][i].getPosition().lat();
            var lng = marker[ind][i].getPosition().lng();
           //map.setCenter(new google.maps.LatLng(lat, lng));
          // myLatlng = new google.maps.LatLng(lat,lng);
          marker[ind][i].setIcon(alternateMarkers[ind][i]);
		  infowindow[ind].open(map, marker[ind][i]);
		    //markers.push(marker[ind][i]);            
        }
        AutoCenter(marker[ind]);
       // infowindow[ind].open(map, marker[ind]);
        //AutoCenter();
        $(this).addClass('background-gray');
        
    },function() {
    	var ind = $(this).attr("data-id");
        for (var i  = 0; i < infowindow.length; i++) {
            infowindow[i].close();
        }
        for(var i = 0; i < Object.size(marker[ind])-1; i++){
        	marker[ind][i].setIcon(markersIcon[ind][i]);
        }
        $(this).removeClass('background-gray');
    });
});

function AutoCenter(markers) {	 
    //  Create a new viewpoint bound
    var bounds = new google.maps.LatLngBounds();
    //  Go through each...
    $.each(markers, function (index, marker) {
    bounds.extend(marker.position);
    });
    //  Fit these bounds to the map
    map.fitBounds(bounds);
  }

@if (Session::get('dt') == 'list')
    var marker = [];
    var infowindow = [];
    var mapPosition = new google.maps.LatLng( {{ DEFAULT_LAT }}, {{ DEFAULT_LNG }});
    var mapOptions = { zoom: 12, center: mapPosition };
    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);    
    
    function initialize() {
        @foreach ($stores as $key => $value)        
        var contentString = '' 
                + '<div style="width: 220px;">'
                + '    <p><b>Name : </b>{{ $value->name }}</p>'
                + '    <p><b>Description : </b>{{ addslashes(json_encode(substr($value->description, 0, 150))) }}</p>'
                + '    <p><i class="fa fa-phone"></i> {{ $value->phone }}&nbsp;&nbsp;&nbsp;'
                + '    <p><b>Address : </b> {{ $value->address }}</p>'
                + '    <p><b>Zip Code : </b> {{ $value->zip_code }}&nbsp;&nbsp;&nbsp;'                
                + '</div>';
                
        infowindow[{{ $key }}] = new google.maps.InfoWindow({
            content: contentString
        });
        var markImg=new google.maps.MarkerImage('http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=|00aeef|000000');
        var altMarkImg=new google.maps.MarkerImage('http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=|ff0000');
        marker[{{$key}}] = [];
        markersIcon[{{$key}}] = [];
        alternateMarkers[{{$key}}] = [];
        @for($i = 0; $i< sizeof($value->officies);$i++) 
        	marker[{{ $key }}][{{$i}}] = new google.maps.Marker({
		            	position: new google.maps.LatLng({{ $value->officies[$i]->office->lat }}, {{ $value->officies[$i]->office->lng }}),  
		            	map: map,
		            	icon: markImg,
		            	title: '{{ $value->name }}' 
		               });
	        google.maps.event.addListener(marker[{{ $key }}], 'mouseover', function() {	        	
	            infowindow[{{ $key }}].open(map, marker[{{ $key }}][{{$i}}]);
	        });    
	        google.maps.event.addListener(marker[{{ $key }}][{{$i}}], 'mouseout', function() {
	            infowindow[{{ $key }}].close();
	        });
	        markersIcon[{{$key}}].push(markImg);
	        alternateMarkers[{{$key}}].push(altMarkImg);
        @endfor
        @endforeach
    }
    google.maps.event.addDomListener(window, 'load', initialize);
@endif
</script>
@stop

@stop
