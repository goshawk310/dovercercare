<?php namespace User;

use Illuminate\Routing\Controllers\Controller;
use View, Input, Redirect, Session, Validator, Request, Response, Mail, URL;
use User as UserModel, City as CityModel, Category as CategoryModel, Cart as CartModel;
use Comment as CommentModel, Contact as ContactModel, Offer as OfferModel, UserOffer as UserOfferModel;
use Feedback as FeedbackModel, Rating as RatingModel, ReviewPhoto as ReviewPhotoModel;
use Company as CompanyModel, Consumer as ConsumerModel, CommonFunction as CommonFunctionModel;
use Store as StoreModel;
use Pclass as PclassModel;
use ProfSubClass as ProfSubClassModel;
use Office as OfficeModel;

class UserController extends \BaseController {
    
    public function login() {
        if (Session::has('user_id')) {
            return Redirect::route('user.home');
        }
        
        $param['pageNo'] = 51;
        if ($alert = Session::get('alert')) {
            $param['alert'] = $alert;
        }
        
        $param['redirect'] = Input::has('redirect') ? Input::get('redirect') : '';
        $param['classes'] = PclassModel::all();
        $param['officies'] = OfficeModel::all();
        $param['categories'] = CategoryModel::all();
        $param['cities'] = CityModel::all();        
        return View::make('user.auth.login')->with($param);        
    }
    
    public function signup() {
        if (Session::has('user_id')) {
            return Redirect::route('user.home');
        }
    
        $param['pageNo'] = 52;
        if ($alert = Session::get('alert')) {
            $param['alert'] = $alert;
        }
        
        $param['redirect'] = Input::has('redirect') ? Input::get('redirect') : '';
        $param['classes'] = PclassModel::all();
        $param['officies'] = OfficeModel::all();
        $param['categories'] = CategoryModel::all();
        $param['cities'] = CityModel::all();        
        return View::make('user.auth.signup')->with($param);
    }    
    
    public function active($token) {
        $user = UserModel::where('token', $token)->firstOrFail();
        $user->is_active = TRUE;
        $user->save();
        
        $alert['msg'] = 'You have successfully active your account';
        $alert['type'] = 'success';
                
        return Redirect::route('user.home')->with('alert', $alert);
    }
    
    public function doLogin() {
        $email = Input::get('email');
        $password = Input::get('password');
        
        $user = UserModel::whereRaw('email = ? and secure_key = md5(concat(salt, ?))', array($email, $password))->get();
        if (count($user) != 0) {
            if ($user[0]->is_active) {
                Session::set('user_id', $user[0]->id);
                Session::set('user_name', $user[0]->name);
                $user = UserModel::find($user[0]->id);
                $user->session_status = 1;
                $user->save();
                if (Input::has('redirect'))
                    return Redirect::to(Input::get('redirect'));
                else
                    return Redirect::route('user.home');
            } else {
                $alert['msg'] = "You must verify your account to login. <button class='btn btn-default pull-right btn-sm' data-id=".$user[0]->id." id='js-btn-resend'>Resend Email</button>";
                $alert['type'] = 'danger';
                return Redirect::route('user.login')->with('alert', $alert);                
            }
        } else {
            $alert['msg'] = 'Invalid Email and Password';
            $alert['type'] = 'danger';
            return Redirect::route('user.login')->with('alert', $alert);
        }
    }

    public function doSignup() {
        $rules = ['email'      => 'required|email',
                  'password'   => 'required|confirmed',
                  'password_confirmation' => 'required',
                  'name'       => 'required',
        ];
        
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()) {
            return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
        } else {
            $is_exist = FALSE;
            $users = UserModel::where('email', Input::get('email'))->get();
            if ($users->count() > 0) {
                $user = $users[0];
                if ($user->secure_key != '') {
                    $is_exist = TRUE;
                }
            }  else {
                $user = new UserModel;
            }
            
            if ($is_exist) {
                $alert['msg'] = 'The account is already exist';
                $alert['type'] = 'danger';           
            } else {
                $user->name = Input::get('name');
                $user->email = Input::get('email');
                $user->phone = Input::get('phone');
                $user->photo = DEFAULT_PHOTO;
                $user->token = strtoupper(str_random(6));
                $user->salt = str_random(8);
                $user->secure_key = md5($user->salt.Input::get('password'));
                $user->is_active = FALSE;
                $user->save();
                
                $param = ['active_link' => URL::route('user.active', $user->token)];
                
                $info = [ 'reply_name'  => REPLY_NAME,
                          'reply_email' => REPLY_EMAIL,
                          'email'       => $user->email,
                          'name'        => $user->name,
                          'subject'     => SITE_NAME,
                    ];
                
                Mail::send('email.active', $param, function($message) use ($info) {
                    $message->from($info['reply_email'], $info['reply_name']);
                    $message->to($info['email'], $info['name'])
                        ->subject($info['subject']);
                });
                
                $alert['msg'] = 'Check your email to verify your account';
                $alert['type'] = 'success';
            }            
            
            return Redirect::route('user.signup')->with('alert', $alert);
        }
    }    
    
    public function doSignout() {
    	$user = UserModel::find(Session::get('user_id'));
    	$user->session_status = 0;
    	$user->save();
        Session::forget('user_id');
        Session::forget('user_name');        
        return Redirect::route('user.home');
    }
    
    public function profile() {
        $param['pageNo'] = 2;
        $param['categories'] = CategoryModel::all();
        $param['cities'] = CityModel::all();
        $param['classes'] = PclassModel::all();
        $param['officies'] = OfficeModel::all();
        $user = UserModel::find(Session::get('user_id'));
        $param['user'] = $user;
        if ($alert = Session::get('alert')) {
            $param['alert'] = $alert;
        }        
        return View::make('user.auth.profile')->with($param);        
    }
    
    public function updateProfile() {
        $rules = ['email'      => 'required|email',
                  'name'       => 'required',
            ];
        
        $user = UserModel::find(Session::get('user_id'));
        $user->email = Input::get('email');
        $user->name = Input::get('name');
        $user->phone = Input::get('phone');
        if (Input::get('password') != '') {
            $user->salt = str_random(8);
            $user->secure_key = md5($user->salt.Input::get('password'));            
        }
        $user->save();
        
        $alert['msg'] = 'User profile has been updated successfully';
        $alert['type'] = 'success';
        
        return Redirect::route('user.profile')->with('alert', $alert);        
    }
    
    public function cart() {
        $param['pageNo'] = 1;
        $param['categories'] = CategoryModel::all();
        $param['cities'] = CityModel::all();
        $param['classes'] = PclassModel::all();
        $param['officies'] = OfficeModel::all();
        $param['carts'] = CartModel::where('user_id', Session::get('user_id'))->paginate(PAGINATION_SIZE);
        
        return View::make('user.carts.index')->with($param);
    }
    

    public function offers() {
        $param['pageNo'] = 3;
        $param['categories'] = CategoryModel::all();
        $param['cities'] = CityModel::all();
        $param['classes'] = PclassModel::all();
        $param['officies'] = OfficeModel::all();
        $param['userOffers'] = UserOfferModel::where('user_id', Session::get('user_id'))->where('is_used', FALSE)->get();
        return View::make('user.offers.index')->with($param);
    }    
    
    public function sendMessage() {
        $store = StoreModel::find(Input::get('store_id'));
        $contact = new ContactModel;
        $contact->company_id = $store->company_id;
        $contact->name = Input::get('name');
        $contact->email = Input::get('email');
        $contact->description = Input::get('description');
        $contact->save();
        
        $alert['msg'] = 'You send contact successfully';
        $alert['type'] = 'success';
        return Redirect::route('store.detail', $store->slug)->with('alert', $alert);
    }
    
    public function giveFeedback() {
        $store = StoreModel::find(Input::get('store_id'));
        $feedbacks = FeedbackModel::where('store_id', $store->id)->where('user_id', Session::get('user_id'));
        if ($feedbacks->count() > 0) {
            // RatingModel::where('feedback_id', $feedbacks[0]->id)->delete();
            $feedbacks->delete();
        }
        
        $feedback = new FeedbackModel;
        $feedback->store_id = $store->id;
        $feedback->user_id = Session::get('user_id');
        $feedback->description = Input::get('description');
        $feedback->save();
        
        foreach (Input::get('rating') as $key => $value) {
            $rating = new RatingModel;
            $rating->feedback_id = $feedback->id;
            $rating->type_id = Input::get('type_id')[$key];
            $rating->answer = $value;
            $rating->save();            
        }
        
        
        $alert['msg'] = 'You left the feedback successfully';
        $alert['type'] = 'success';
    
        CommonFunctionModel::addConsumer($store->company->id, Session::get('user_id'), 0);
        CommonFunctionModel::addOffer($store->company->id, Session::get('user_id'));
    
        return Redirect::route('store.detail', $store->slug)->with('alert', $alert);
    }
    
    public function uploadPhoto() {
        $store = CompanyModel::find(Input::get('store_id'));
        if (Input::hasFile('photo')) {
            $filename = str_random(24).".".Input::file('photo')->getClientOriginalExtension();
            Input::file('photo')->move(ABS_REVIEW_PATH, $filename);
    
            $reviewPhoto = new ReviewPhotoModel;
            $reviewPhoto->photo = $filename;
            $reviewPhoto->description = Input::get('description');
            $reviewPhoto->store_id = $store->id;
            $reviewPhoto->user_id = Session::get('user_id');
            $reviewPhoto->save();
            $alert['msg'] = 'Image has been uploaded successfully';
            $alert['type'] = 'success';
        } else {
            $alert['msg'] = 'Please select file to upload';
            $alert['type'] = 'danger';
        }       
    
        return Redirect::route('store.detail.photo', $company->slug)->with('alert', $alert);
    }
    
    public function asyncAddCart() {
    	$flag = 1;    	
    	if((Input::get('price') == 'HP' && !Input::has('duration'))) $flag = 0;
        if (Input::has('store_id') && Session::has('user_id')  && Input::has('when') && $flag == 1 ) {
            
                $cart = new CartModel;
                $cart->store_id = Input::get('store_id');
                $cart->user_id = Session::get('user_id');
                $cart->book_date = Input::get('when');
                $cart->duration = Input::get('duration');
                $cart->company_id = Input::get('company_id');
                $cart->price = Input::get('price');
                $cart->message = Input::get('msg');
                $cart->user_address = Input::get('addr');
                $cart->office_id = Input::get('office_id');
                $cart->save();
                if(!empty(Input::get('addr'))){
                	$location = 'Home';
                	$addr = Input::get('addr');
                }else{
                	$location = $cart->office->name;
                	$addr = $cart->office->address;
                }
                // send mail to customer.
                $books = CartModel::where('store_id', Input::get('store_id'))->where('user_id', Session::get('user_id'))->firstOrFail();                
                $param = ['book_date' => Input::get('when'),
                		  'duration' => Input::get('duration'),
        				  'store_name' => $books->store->name,
        				  'user_name' => $books->user->name,
        				  'price'=>$books->price,
        				  'msg' => Input::get('msg'),
        				  'addr' => $addr,
        				  'location' => $location,
        				];
                
                $info = [ 'reply_name'  => REPLY_NAME,
                          'reply_email' => REPLY_EMAIL,
                          'email'       => $books->user->email,
                          'name'        => $books->user->name,
                          'subject'     => SITE_NAME,
                    ];
                
                Mail::send('email.makeBook', $param, function($message) use ($info) {
                    $message->from($info['reply_email'], $info['reply_name']);
                    $message->to($info['email'], $info['name'])
                        ->subject($info['subject']);
                });
                // \End send mail to customer
                // Send mail to Store.
                	$info = [   'reply_name'  => REPLY_NAME,
			                	'reply_email' => REPLY_EMAIL,
			                	'email'       => $books->store->company->email,
			                	'name'        => $books->store->name,
			                	'subject'     => SITE_NAME,
                	];
                	Mail::send('email.bookNoti', $param, function($message) use ($info) {
                		$message->from($info['reply_email'], $info['reply_name']);
                		$message->to($info['email'], $info['name'])
                		->subject($info['subject']);
                	});
                // \ End Send mail to store
                $result['result'] = "success";
                $result['msg'] = "Your book has been added on cart successfully.";
                return Response::json($result, 200);
          
        } else {
            $result['result'] = "failed";
            if(!Session::has('user_id')){
            	$result['msg'] = "You have to login";
            }elseif(Input::get('price') == 'HP' && (!Input::has('duration') || !Input::has('when')) ){            	
            	$result['msg'] = "Please pick a date and duration";
            }elseif(Input::get('price') != 'HP' && !Input::has('when') ){            	
            	$result['msg'] = "Please pick a date and duration";
            }else
                $result['msg'] = "Invalid Request";
            return Response::json($result, 200);
        }
    }
    
    public function asyncRemoveCart() {
        CartModel::find(Input::get('cart_id'))->delete();
        $result['result'] = "success";
        $result['msg'] = "The company has been removed succssfully from the cart";
        return Response::json($result, 200);
    }
}
