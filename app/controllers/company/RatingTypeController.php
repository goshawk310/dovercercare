<?php namespace Company;

use Illuminate\Routing\Controllers\Controller;
use View, Input, Redirect, Session, Validator;
use RatingType as RatingTypeModel;

class RatingTypeController extends \BaseController {
    public function index() {
        $param['ratingTypes'] = RatingTypeModel::where('company_id', Session::get('company_id'))->paginate(PAGINATION_SIZE);
        $param['pageNo'] = 11;
        if ($alert = Session::get('alert')) {
            $param['alert'] = $alert;
        }
        
        return View::make('company.ratingType.index')->with($param);
    }
    
    public function create() {
        $param['pageNo'] = 11;
        return View::make('company.ratingType.create')->with($param);
    }
    
    public function edit($id) {
        $param['pageNo'] = 11;
        $param['ratingType'] = RatingTypeModel::find($id);
        
        return View::make('company.ratingType.edit')->with($param);
    }
    
    public function store() {
        
        $rules = ['name' => 'required', ];
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        } else {
            if (Input::has('rating_type_id')) {
                $id = Input::get('rating_type_id');
                $ratingType = RatingTypeModel::find($id);
            } else {
                $ratingType = new RatingTypeModel;                
            }
            $ratingType->company_id = Session::get('company_id');
            $ratingType->name = Input::get('name');
            $ratingType->is_visible = Input::get('is_visible');
            $ratingType->is_score = Input::get('is_score');
            $ratingType->save();
            
            $alert['msg'] = 'Rating Type has been saved successfully';
            $alert['type'] = 'success';            
              
            return Redirect::route('company.ratingType')->with('alert', $alert);            
        }
    }
    
    public function delete($id) {
        try {
            RatingTypeModel::find($id)->delete();
            $alert['msg'] = 'Rating Type has been deleted successfully';
            $alert['type'] = 'success';            
        } catch(\Exception $ex) {
            $alert['msg'] = 'This Rating Type has been already used';
            $alert['type'] = 'danger';
        }

        return Redirect::route('company.ratingType')->with('alert', $alert);
    }
}
