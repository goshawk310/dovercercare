@extends('user.layout')

@section('custom-styles')
{{ HTML::style('/assets/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}
{{ HTML::style('/assets/metronic/frontend/onepage/css/custom.css') }}
{{ HTML::style('/assets/metronic/global/plugins/bootstrap-modal/css/bootstrap-modal.css') }}
{{ HTML::style('/assets/css/bootstrap-checkbox.css') }}
<style>
.faq-tabbable i {
    color: #000 !important;
}
</style>
@stop

@section('main')
<div class="container">
    <div class="row">
        <div class="col-sm-3 margin-top-sm">
            @include('user.store.info')
        </div>
        <div class="col-md-9 margin-top-normal">
            
            @if (isset($alert))
            <div class="alert alert-{{ $alert['type'] }} alert-dismissibl fade in">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <p>
                    {{ $alert['msg'] }}
                </p>
            </div>
            @endif
            
            @include('user.store.topMenu')		
            <div class="page-content">
                <div class="tab-content">
                    <div class="tab-pane active" id="company-profile">
                        <div class="row">
                            <div class="col-sm-8">
                                <h2 class="color-default pull-left">{{ $store->name }}</h2>
                                <div class="pull-right">
                                    <input id="js-number-rating" type="number" class="rating" min=0 max=5 step=1 data-show-clear=false data-show-caption=false data-size='xs' value="{{ $store->getRatingScore() }}" readonly=true style="cursor: pointer;">                                
                                </div>
                                <div class="clearfix"></div>
                                <div class="blog-tags margin-bottom-20">
                                    <ul>
                                        @foreach ($store->subCategories as $key => $value)
                                        <li><a href="{{ URL::route('store.search').'?keyword='.$value->subCategory->name }}"><i class="fa fa-tags"></i> {{ $value->subCategory->name }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                                                        
                                <h4 class="margin-top-sm">About Us</h4>
                                <p>
                                    {{ $store->description }}
                                </p>
                                <hr/>
                                <p>
                                    <img class="img-responsive img-rounded" alt="" src="{{ HTTP_STORE_PATH.$store->photo }}">
                                </p>                               
                            </div>
                            <div class="col-sm-4">
                            	<div class="col-xs-12 prices-block"><!-- Booking form -->
                            		<div class="pricing-item">
							            <div class="pricing-head testimonials-v1">
							              <div class="carousel-info">
							                <a href="{{ URL::route('company.detail', $store->company->slug) }}" style="color:#ffffff;" data-html="true">
							                	<img class="" src="{{ HTTP_COMPANY_PATH.$store->company->photo }}" alt="">							                    
							              	</a>
							              </div>
							              <p style="margin-top:10px;"><a href="{{ URL::route('company.detail', $store->company->slug) }}" style="color:#ffffff;"  data-html="true">
					                            {{ $store->company->name }}
					                        </a></p>
							            </div>
							            <div class="pricing-content">							            	
							             
			                                <p><b>Phone : </b>{{ $store->phone }}</p>
			                                <div class="pi-price" style="text-align:center;">
								                <strong>&#8364;<em>{{ $store->price_value }}</em></strong>
								                <input type="hidden" id="price" value="{{ $store->price_value }}"/>
								                <p>@if($store->price == 'HP') Per Hour @else Full Price @endif</p>
								            </div>
								            <div class="col-md-12 checkbox-list" style="padding:0px; margin-bottom:5px;">
								            <div class="checkbox-list" style="background-color:#da4f49;">
								            	<label style="color:#fff;">
													<input type="checkbox" id="home"> At My Home </label>
								            	</div>
								            </div>
								           
								            <!--  Office Setting imak 20150610 -->
								            <?php $serviceOfficies = []; ?>
								            @if(count($personal_officies)>1)
								            <div class="col-md-12" style="padding:0px; margin-bottom:20px;">
			                                	<select class="form-control" id="service_office">
			                                	<?php $i = 0; ?>
			                                		@foreach ($personal_officies as $office)	
			                                		<?php $serviceOfficies[] = $office->office->opening; ?>		                                		
			                                		<option value="{{$office->office->id}}" {{$i == 0 ? 'selected' : ''}} office-index ="{{$i}}">{{$office->office->name}}</option>
			                                		<?php $i++;?>
			                                		@endforeach
			                                	</select>
			                                </div>
			                                @else
			                                <?php $serviceOfficies[] = $personal_officies[0]->office->opening; ?>
			                                @endif
								            <!-- / Office Setting imak 20150610 -->
								            <!-- DateTimePicker for Booking -->
			                                <div class="col-md-12" style="padding:0px; margin-bottom:20px;">
												<div class="input-group date form_datetime">
													<input type="text" size="16" readonly="" class="form-control"  placeholder="When?" id="date_picker">
													<span class="input-group-btn">
													<!-- <button class="btn default date-reset" type="button"><i class="fa fa-times"></i></button> -->
													<button class="btn default date-set" type="button" onmousedown="jQuery('#date_picker').trigger('mousedown');"><i class="fa fa-calendar"></i></button>
													</span>
												</div>																			
											</div>
											<!-- \End DateTimePicker for Booking -->
			                                <!-- Duration Setting imak 20150520-->
			                                @if($store->price == 'HP')
			                                <div class="col-md-12" style="padding:0px; margin-bottom:20px;">
			                                	<?php 
			                                		$duration = explode(',',$store->duration);
			                                	?>
			                                	
			                                	<select class="form-control" id="duration">
			                                		<option value="" disabled selected>Duration:</option>
			                                		@foreach ($duration as $d)
			                                		<option value={{$d}}>{{$d}}</option>
			                                		@endforeach
			                                	</select>
			                                </div>
			                                @endif
			                                <!-- / End Duration Setting imak 20150520-->
			                               
			                                <div id="map-canvas" style="height: 200px; width: 100%;" class="margin-top-xs"></div>
			                                <p class="text-center margin-top-xs">
			                                    <i class="fa fa-map-marker"></i>
			                                    <b>{{ $store->address }}</b>
			                                </p>							              
							            </div>
							            <div class="pricing-footer">
							              <a  class="btn btn-default" id="book_btn" data_id="{{ Session::has('user_id') ? Session::get('user_id') : ''  }}"  href="#basic">Book!</a>
							              <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
															<h4 class="modal-title">Your Booking</h4>
														</div>
														<div class="modal-body">
															<form>															
																<div class="row">
																	<div class="col-md-5">
																		<span class="col-md-12 form-control">Book Date:</span>
																		@if($store->price == "HP")
																		<span class="col-md-12 form-control">Duration:</span>
																		@endif
																		<span class="col-md-12 form-control">Price(&#8364;):</span>
																	</div>
																	<div class="col-md-7">
																		<input type="text" readonly id="modal_date" class="col-md-12 form-control">
																		@if($store->price == "HP")
																		<input type="text" readonly  id="modal_duration" class="col-md-12 form-control">
																		@endif
																		<input type="text" readonly  id="modal_price" class="col-md-12 form-control">
																	</div>
																	<div id="user_addr" class="col-md-12" style="margin-top:20px;">
																		<input class="form-control" type="text" placeholder="Type your address" name="user_addr" style="display:none;" required>
																		<textarea rows="5" placeholder="Type your message..." name="user_msg" class="form-control" style="margin-top:10px;"></textarea>
																	</div>
																	
																</div>																
															</form>
															</div>
														<div class="modal-footer">
															<button type="button" id="js-btn-add-cart" class="btn btn-primary" data-dismiss="modal" company_id="{{ $store->company->id }}" data-id="{{ $store->id }}" data-user="{{ Session::has('user_id') ? Session::get('user_id') : '' }}">Pay to the office</button>
															<button type="button" id="js-btn-pay-now" class="btn btn-primary" company_id="{{ $store->company->id }}" data-id="{{ $store->id }}" data-user="{{ Session::has('user_id') ? Session::get('user_id') : '' }}">Pay Now</button>
														</div>
														</div>
														
													</div>
													<!-- /.modal-content -->
												</div>
												<!-- /.modal-dialog -->
											</div>
							            </div>
							          </div>
                            	</div><!-- / End Booking form -->
                            	
                                <div class="company-service margin-bottom-normal">
			                                    <h4>Keywords</h4>
			                                    <?php $keywords = explode(",", $store->keyword); ?>
			                                    @foreach ($keywords as $key => $value)
			                                    <button class="btn-default btn btn-xs">{{ $value }}</button>
			                                    @endforeach
			                     </div>                            
                            </div>                            
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-sm-5 col-sm-offset-1">
                                <button class="btn btn-primary btn-lg btn-block" id="js-btn-join" data-id="{{ $store->company->id }}"><i class="fa fa-user"></i> Join As Customer</button>
                            </div>
                            <div class="col-sm-5">
                                <button class="btn btn-primary btn-lg btn-block" id="js-btn-send-message"><i class="fa fa-pencil-square-o"></i> Send A Message</button>
                            </div>
                        </div>
                        <div class="row margin-top-normal" id="js-div-message" style="display: none;">
                            <div class="col-sm-10 col-sm-offset-1">
                                <h4 class="color-default">Send Us A Message</h4>
                                <form action="{{ URL::route('user.sendMessage') }}" role="form" method="post">
                                    <input type="hidden" name="store_id" value="{{ $store->id }}"/>
                                    <div class="form-group">
                                        <label for="contacts-name">Name</label>
                                        <input type="text" class="form-control" name="name">
                                    </div>
                                    <div class="form-group">
                                        <label for="contacts-email">Email</label>
                                        <input type="email" class="form-control" name="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="contacts-message">Message</label>
                                        <textarea class="form-control" rows="5" name="description"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-primary pull-right"><i class="icon-ok"></i> Send</button>
                                </form>                            
                            </div>
                        </div>
                        @if (count($store->company->purchaseOffers) != 0)
                            <h2 class="margin-top-normal">Offers</h2>
                            <div class="row">
                            @foreach ($store->company->purchaseOffers as $key => $offer)
                                <div class="col-sm-4 text-center">
                                    <div class="service-item">
                                        <h4>{{ $offer->name }}</h4>
                                        <div style="height: 42px; overflow: hidden;"><p>{{ $offer->description }}</p></div>
                                        <h3 class="color-default">{{ $offer->price.'&euro;'}}</h3>
                                        <div class="row">
                                            <button class="btn btn-primary col-sm-8 col-sm-offset-2" id="js-btn-purchase" data-price="{{ $offer->price }}" data-id="{{ $offer->id }}" data-user="{{ Session::has('user_id') ? Session::get('user_id') : '' }}">
                                                <i class="fa fa-heart"></i> Purchase
                                            </button>
                                        </div>
                                        <p class="padding-top-xs"><b><i>Expires At : {{ $offer->expire_at }}</i></b></p>
                                    </div>
                                </div>
                            @endforeach
                            </div>
                        @endif
                        
                        <div class="row">
                            <div class="col-sm-12"><hr/></div>
                        </div>
                        
                        @if (count($store->company->loyalties) != 0)
                        <h2 class="margin-top-lg">Loyalties</h2>
                            <div class="row">
                            @foreach ($store->company->loyalties as $key => $loyalty)
                                <div class="col-sm-4 text-center">
                                    <div class="service-item">
                                        <h4>{{ $loyalty->name }}</h4>
                                        <p>{{ $loyalty->description }}</p>
                                        <div class="row">
                                            <button class="btn btn-primary col-sm-8 col-sm-offset-2">
                                                <i class="fa fa-money"></i> {{ $loyalty->count_visit." Stamps" }}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </div>
                        @endif                        
                        
                        @if ($is_valid || count($store->feedbacks) > 0)
                        <h2 class="margin-top-normal">Review &amp; Feedbacks</h2>
                        @endif
                        
                        @if ($is_valid)
                        <form method="post" action="{{ URL::route('user.giveFeedback') }}">
                            <input type="hidden" name="store_id" value="{{ $store->id }}"/>
                            <div class="row margin-top-xs">
                                <div class="col-sm-6">
                                    @foreach ($store->company->visibleRatingTypes as $key => $value)
                                    @if ($value->is_score)
                                    <div class="row">
                                        <div class="col-sm-6 text-right"><p style="padding-top: 10px;"><b>{{ $value->name }} : </b></p></div>
                                        <div class="col-sm-6">
                                            <input id="js-number-rating" type="number" name="rating[]" class="rating" min=0 max=5 step=1 data-show-caption=false data-show-clear=false data-size='xs' value=3>
                                            <input type="hidden" name="type_id[]" value="{{ $value->id }}">
                                        </div>
                                    </div>
                                    @else
                                    <div class="row">
                                        <div class="form-group form-radio">
                                            <div class="col-sm-6 text-right"><p style="padding-top: 10px;"><b>{{ $value->name }}</b></p></div>
    										<div class="col-md-6 padding-top-xs">
    										    <input type="hidden" name="type_id[]" value="{{ $value->id }}">
    										    <input type="hidden" name="rating[]" id="js-hidden-rating">
    											<div class="btn-group" data-toggle="buttons">
													<label class="btn btn-default btn-sm" data-val=1 id="js-label-answer"><input type="radio" class="toggle"> Yes </label>
													<label class="btn btn-default btn-sm" data-val=0 id="js-label-answer"><input type="radio" class="toggle"> No </label>
												</div>
    										</div>
									    </div>
                                    </div>
                                    @endif
                                    @endforeach
                                </div>
                                <div class="col-sm-6">
                                    <textarea class="form-control" name="description" rows="9" placeholder="Write feedback here..."></textarea>
                                    <button class="btn btn-primary pull-right margin-top-sm" onclick="return validate()">Give Feedback</button>
                                </div>
                            </div>
                        </form>
                        @endif
                        
                        @if (count($store->feedbacks) > 0)
                        <table class="table table-hover margin-top-lg">
                            <thead>
                                <tr>
                                    <th style="width: 120px;"></th>
                                    @foreach ($store->company->visibleRatingTypes as $ratingType)
                                    <th class="text-center">{{ $ratingType->name }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($store->feedbacks as $feedback)
                            <tr>
                                <td rowspan="2">
                                    By {{ $feedback->user->name }}
                                    <p class="margin-top-xs">
                                        <i class="fa fa-clock-o"></i>&nbsp;{{ date(DATE_FORMAT, strtotime($feedback->created_at)) }}
                                    </p>
                                </td>
                                @foreach ($store->company->visibleRatingTypes as $ratingType)
                                <td class="text-center">
                                    @if ($ratingType->is_score)
                                    <input id="js-number-rating" type="number" class="rating" min=0 max=5 step=1 data-show-clear=false data-show-caption=false data-size='xs' value="{{ $feedback->getTypeScore($ratingType->id) }}" readonly=true>
                                    @else
                                        {{ $feedback->getTypeScore($ratingType->id) == -1 ? '--' :  ($feedback->getTypeScore($ratingType->id) == 0 ? 'No' : 'Yes') }}
                                    @endif                                
                                </td>
                                @endforeach
                            </tr>
                            <tr>
                                <td colspan="{{ count($store->company->visibleRatingTypes) }}" style="border-top: none; padding-top: 0px; padding-bottom: 5px;"><i>{{ $feedback->description }}</i></td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>            
    </div>
</div>

<form id="js-frm-payment" method="post" action="{{ 'https://'.PAYPAL_SERVER.'/cgi-bin/webscr' }}" class="hide">
	<input type="hidden" name="business" value="{{ $store->company->paypal_id }}" />
	<input type="hidden" name="cmd" value="_xclick">
	<input type="hidden" name="item_name" value="{{ SITE_NAME }} Offer Purchase">
	<input type="hidden" name="amount">
	<input type="hidden" name="custom" >
	<input type="hidden" name="currency_code" value="EUR">
	<input type="hidden" name="notify_url" value="{{ URL::route('offer.purchase.ipn') }}">
	<input type="hidden" name="return" value="{{ URL::route('offer.purchase.success', $store->slug) }}">
	<input type="hidden" name="cancel_return" value="{{ URL::route('offer.purchase.failed', $store->slug) }}">
	<input type="hidden" name="no_shipping" value="1">
	<input type="hidden" name="email">
</form>
<!-- Booking paypal -->
<form id="js-book-payment" method="post" action="{{ PAYPAL_URL }}" class="hide">
	<input type="hidden" name="business" value="{{ $store->company->paypal_id }}">
	<input type="hidden" name="cmd" value="_xclick">
	<input type="hidden" name="item_name" value="{{ $store->name }}">
	<input type="hidden" name="user_id" value="{{ Session::has('user_id') ? Session::get('user_id') : ''  }}"/>
	<input type="hidden" name="store_id" value="{{ $store->id }}" />
	<input type="hidden" name="company_id" value="{{ $store->company->id }}" />
	<input type="hidden" name="duration"/>
	<input type="hidden" name="book_date"/>
	<input type="hidden" name="amount">
	<input type="hidden" name="custom" >
	<input type="hidden" name="currency_code" value="EUR">
	<input type="hidden" name="notify_url" value="{{ URL::route('book.purchase.ipn') }}">
	<input type="hidden" name="return" value="{{ URL::route('book.purchase.success', $store->slug) }}">
	<input type="hidden" name="cancel_return" value="{{ URL::route('book.purchase.failed', $store->slug) }}">
	<input type="hidden" name="no_shipping" value="1">
	<input type="hidden" name="email">
</form>
<?php 
	$lat = [];
	$lng = [];
	if( empty( $personal_officies ) )
	{
		$lat[] = DEFAULT_LAT;
		$lng[] = DEFAULT_LNG;
	}else{
		foreach($personal_officies as $office){
			$lat[] = $office->office->lat;
			$lng[] = $office->office->lng;
		}	
	}
	
?>
<script type='text/javascript'>
<?php
$lat = json_encode($lat);
$lng = json_encode($lng);
echo "var lat = ". $lat . ";\n";
echo "var lng = ". $lng . ";\n";
?>
</script>
@stop

@section('custom-scripts')
{{ HTML::script('/assets/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}
{{ HTML::script('/assets/metronic/global/plugins/bootstrap-modal/js/bootstrap-modal.js') }}
{{ HTML::script('/assets/metronic/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}
{{ HTML::script('/assets/js/bootstrap-checkbox.js') }}
<script>
$(document).ready(function() {
    $("label#js-label-answer").click(function() {
        $(this).parents("div.form-radio").find("input#js-hidden-rating").val($(this).attr('data-val'));
    });
    
    $('#book_btn').click(function(){
        var user_id = $(this).attr('data_id');
        if(user_id == ''){
        	var msg = 'You need to login';
        	bootbox.alert(msg);
            window.setTimeout(function(){
                bootbox.hideAll();
            }, 2000); 
            return;
        }else if( $('#date_picker').val() == '' || ($('#duration').val() == null && $('#duration').length > 0)){
            var msg = 'You need to select booking date and duration';
        	bootbox.alert(msg);
            window.setTimeout(function(){
                bootbox.hideAll();
            }, 2000); 
            return;
        }
        
        if($('#home').prop('checked'))
            $('input[name="user_addr"]').css('display','block');
        else
        	$('input[name="user_addr"]').css('display','none');
    	$('#basic').modal('show');
    	var price_type = '<?php echo $store->price; ?>';
    	var price = Number($('#price').val());
    	var book_date = $('#date_picker').val();
    	if ($('#duration').length > 0) {
    		var duration = $('#duration').val();
        	duration_temp = duration.split('h');
        	duration = Number(duration_temp[0]);
        	$('#modal_duration').val(duration);
            $('input[name="duration"]').val(duration);
    	}
    	
    	if(price_type == 'HP'){
			price = price*duration;
        }
        $('#modal_price').val(price);
        $('input[name="amount"]').val(price);        
        $('input[name="book_date"]').val(book_date);
        $('#modal_date').val(book_date);
    });    
});

function validate() {
    var objList = $("input#js-hidden-rating");
    for (var i = 0; i < objList.length; i++) {
        if (objList.eq(i).val() == '') {
            bootbox.alert("Please answer the question.");
            return false;
        }
    }
}

var dateToday = new Date();
var daysofweek = ["sun","mon","tue","wed","thu","fri","sat"];
var start_time,end_time;
var storeId = '<?php echo $store->id;?>';
var allow_time = new Array();
var book_time = new Array();
var days_opening;
var openTimes =<?php echo json_encode($serviceOfficies );?>;
days_opening = openTimes[0];
allow_time = getAllowTime(dateToday);


$(document).ready(function() {
	$('input[type="checkbox"]#home').checkbox({
	    buttonStyle: 'btn-danger',
		buttonStyleChecked: 'btn-base',
	    checkedClass: 'icon-check',
	    uncheckedClass: 'icon-check-empty'
	});

	$('#home').change(function(){
		if($(this).prop('checked')){
			$('#service_office').hide();
		}else{
			$('#service_office').show();
		}
	});
	// DateTimePicker Imak 20150517	
	
	$('#date_picker').datetimepicker({
		minDate:dateToday,
		onSelectDate:function(ct,$input){
			$('#date_picker').datetimepicker({
				minDate:dateToday,
				allowTimes:getAllowTime(ct)
			});
			
		},
		allowTimes:allow_time
	}); 
	
   // \ End DateTimePicker Imak 20150517
   
  //choose duration via office
 
  //if($('#service_office').length > 0){
	  $('#service_office').on('change', function (e) {
		    var selectedValue = $("option:selected", this).attr('office-index');
		   	days_opening = openTimes[selectedValue];	   	
		});
	//}
  // \ choose duration via office
    $("button#js-btn-add-cart").click(function() {
		var d = '';
		var office_id = '';
         if($('#duration').length > 0)
        	d = $('#duration').val();
        var w = $('#date_picker').val();
        var p = $('#modal_price').val();
        var addr = $('input[name="user_addr"]').val();
        if(addr == '' && $('#home').prop('checked')){
        	bootbox.alert('Please Input your address');
            window.setTimeout(function(){
                bootbox.hideAll();
            }, 3000);
            return false;
         }
        if($('#home').prop('checked') == false){
			office_id = $('#service_office').val();
         }
        var msg = $('textarea[name="user_msg"]').val();
        $.ajax({
            url: "{{ URL::route('async.user.cart.add') }}",
            dataType : "json",
            type : "POST",
            data : { 
                store_id : $(this).attr('data-id'),
                duration:d,
                when:w,
                company_id : $(this).attr('company_id'),
                price : p,
                addr : addr,
                msg : msg,
                office_id : office_id
                },
            success : function(data){
                bootbox.alert(data.msg);
                window.setTimeout(function(){
                    bootbox.hideAll();
                }, 2000);                   
            }
        });        
    });

    $("div.service-item").hover(function() {
        $(this).addClass('service-item-hover');
    },function() {
        $(this).removeClass('service-item-hover');
    });

    $("button#js-btn-send-message").click(function() {
        if ($("div#js-div-message").css("display") == "none") {
            $("div#js-div-message").show();
        } else {
            $("div#js-div-message").hide();
        }
    });

    $("button#js-btn-purchase").click(function() {
        var userId = $(this).attr('data-user');
        var offerId = $(this).attr('data-id');
        var price = $(this).attr('data-price');
        if (userId == '') {
            window.location.href = "{{ URL::route('user.login').'?redirect='.urlencode(URL::route('store.detail', $store->slug)) }}";
        } else {
            var custom = '{"uid":' + userId + ',"oid":"' + offerId + '"}';
            $("input[name='custom']").val(custom);
            $("input[name='amount']").val(price);
            $("form#js-frm-payment").submit();
        }
    });

	$("button#js-btn-pay-now").click(function(){
		var cmpId = $(this).attr('company_id');
		var storeId = $(this).attr('data-id');
		var userId = $(this).attr('data-user');
		var book_date = $("#date_picker").val();
		var duration = '';
		var addr = $('input[name="user_addr"]').val();
        if(addr == '' && $('#home').prop('checked')){
        	bootbox.alert('Please Input your address');
            window.setTimeout(function(){
                bootbox.hideAll();
            }, 3000);
            return false;
         }
        if($('#home').prop('checked') == false){
			office_id = $('#service_office').val();
         }
        var msg = $('textarea[name="user_msg"]').val();
		if($("#duration").length > 0){
			duration = $("#duration").val();
		}
		var custom = '{"uid":' + userId + ',"cid":' + cmpId + ',"sid":'+storeId+',"book_date":"'+book_date+'","duration":"'+duration+'", "addr":"'+addr+'", "msg" : "'+msg+'", "office_id": "'+office_id+'"}';
		console.log(custom);
        $("input[name='custom']").val(custom);
		$("form#js-book-payment").submit();
	});
	
    $("button#js-btn-join").click(function() {
        $.ajax({
            url: "{{ URL::route('async.user.store.join') }}",
            dataType : "json",
            type : "POST",
            data : { company_id : $(this).attr('data-id') },
            success : function(data){
                bootbox.alert(data.msg);
                window.setTimeout(function(){
                    bootbox.hideAll();
                }, 2000);
            }
        }); 
    });
    var markers = new Array();
    var mapOptions = { zoom: 15 };
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    for(var i=0;i<lat.length;i++){
    	var mapPosition = new google.maps.LatLng(lat[i], lng[i]);    
        var marker = new google.maps.Marker({position: mapPosition, map: map, title: '{{ $store->name }}'});
        markers.push(marker);
    }
    AutoCenter();
    function AutoCenter() {
        //  Create a new viewpoint bound
        var bounds = new google.maps.LatLngBounds();
        //  Go through each...
        $.each(markers, function (index, marker) {
        bounds.extend(marker.position);
        });
        //  Fit these bounds to the map
        map.fitBounds(bounds);
      }
});

function getAllowTime(ct){
	
	book_time = [];
	var day_index = ct.getUTCDay();
	var dd = ct.getDate();
	var mm = ct.getMonth(); //January is 0!
	var yyyy = ct.getFullYear();
	var start_time,end_time;
	var day = daysofweek[day_index];
	var yyyy_str = ct.getFullYear().toString();
	var mm_str = (ct.getMonth()+1).toString(); // getMonth() is zero-based
	var dd_str  = ct.getDate().toString();
	var bookDate =  yyyy_str + '/' + (mm_str[1]?mm_str:"0"+mm_str[0]) + '/' + (dd_str[1]?dd_str:"0"+dd_str[0]);
	
	$.ajax({
		url : "{{URL::route('async.user.bookTime')}}",
		dataType : "json",
		type : "POST",
		data : {
			ct : bookDate,
			storeId : storeId
			},
		success : function (data){
			if($('#home').prop('checked')){
				start_time = "00:00";
				end_time = "23:00";
				}else{
					for (var key in days_opening) {
						   if (days_opening.hasOwnProperty(key) && day+"_start" == key) {
						       start_time = days_opening[key];						       
						    }
						   if (days_opening.hasOwnProperty(key) && day+"_end" == key) {
						       end_time = days_opening[key];						       				       
						    }
						}
				}
			start_time = start_time.split(':');
			start_time = new Date(yyyy,mm,dd,start_time[0],start_time[1]);
			end_time = end_time.split(':');
			end_time = new Date(yyyy,mm,dd,end_time[0],end_time[1]);
			allow_time = [];
			for(var i = start_time.getHours();i<=end_time.getHours();i++) { // getting allow time 
				if(start_time.getMinutes() == 0){
					allow_time.push(i.toString() + ':00');				
				}else if(start_time.getMinutes() == 30){
					if( i == start_time.getHours() ){
						allow_time.push(i.toString() + ':30');
						continue;
					}
					else allow_time.push(i.toString() + ':00');
				}
				if(i != end_time.getHours())
					allow_time.push(i.toString() + ':30');
				if(end_time.getMinutes() == 30 && i == end_time.getHours()){
					allow_time.push(i.toString() + ':30');
				}
			} // \ end getting allow time
			for(var i = 0; i<= data.length - 1; i++){
				var d = data[i].book_date;
				d = new Date(d);
				var h = addZero(d.getHours());
			    var m = addZero(d.getMinutes());
				//book_time.push(h+':'+m);
				var item = h+':'+m;
			    var index = allow_time.indexOf(item);
				allow_time.splice(index, 1);
			}	
			$('#date_picker').datetimepicker({
				minDate:dateToday,
				onSelectDate:function(ct,$input){
					$('#date_picker').datetimepicker({
						minDate:dateToday,
						allowTimes:getAllowTime(ct)
					});
					
				},
				allowTimes:allow_time
			});	
			return allow_time;
		} // \ end success function
	}); // \ end ajax				
}

function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}
</script>
@stop

@stop
