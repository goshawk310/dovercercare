<?php namespace Company;

use Illuminate\Routing\Controllers\Controller;
use View, Input, Redirect, Session, Validator, Response, Request, Mail;
use Company as CompanyModel;

class ContactController extends \BaseController {

    public function index() {
        $param['pageNo'] = 15;
        $param['company'] = CompanyModel::find(Session::get('company_id'));
        return View::make('company.contact.index')->with($param);
    }
}
