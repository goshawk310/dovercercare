<?php namespace Company;

use Illuminate\Routing\Controllers\Controller;
use View, Input, Redirect, Session, Validator, File;
use Store as StoreModel, Company as CompanyModel, City as CityModel;
use Category as CategoryModel, SubCategory as SubCategoryModel, StoreSubCategory as StoreSubCategoryModel;
use Office as OfficeModel, OfficeOpening as OfficeOpeningModel;
class OfficeController extends \BaseController {   
    public function index() {
        $param['pageNo'] = 5;
        $param['officies'] = OfficeModel::where('company_id', Session::get('company_id'))->get();
        
        if ($alert = Session::get('alert')) {
            $param['alert'] = $alert;
        }
        
        return View::make('company.office.index')->with($param);
    }
    
    public function create() {
    	$param['pageNo'] = 5;
        return View::make('company.office.create')->with($param);
    }
    
    public function edit($id) {        
        $param['office'] = OfficeModel::find($id);
        $param['pageNo'] = 5;                
        return View::make('company.office.edit')->with($param);
    }
    
    public function store() {
        $company = CompanyModel::find(Session::get('company_id'));
        $rules = ['name' => 'required'];
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        } else {
            if (Input::has('office_id')) {
                $id = Input::get('office_id');
                $office = OfficeModel::find($id);
                $officeOpening = OfficeModel::find($id)->opening;
            }else{
            	$office = new OfficeModel;
            	$officeOpening = new OfficeOpeningModel;
            }
                        
            $office->company_id = Session::get('company_id');
            $office->name = Input::get('name');           
            $office->address = Input::get('address');
            $office->lat = Input::get('lat');
            $office->lng = Input::get('lng');           
            $office->save();
            
            $officeOpening->office_id = $office->id;
            foreach (['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun', ] as $key) {
            	$officeOpening->{ $key.'_start'} = Input::get($key.'_start');
            	$officeOpening->{ $key.'_end'} = Input::get($key.'_end');
            }
            $officeOpening->save();
            
            $alert['msg'] = 'Office has been saved successfully';
            $alert['type'] = 'success';            
              
            return Redirect::route('company.office')->with('alert', $alert);            
        }
    }
    
    public function delete($id) {
        try {
            OfficeModel::find($id)->delete();
            
            $alert['msg'] = 'Store has been deleted successfully';
            $alert['type'] = 'success';            
        } catch(\Exception $ex) {
            $alert['msg'] = 'This Office has been already used';
            $alert['type'] = 'danger';
        }

        return Redirect::route('company.office')->with('alert', $alert);
    }
}
