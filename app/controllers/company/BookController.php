<?php namespace Company;

use Illuminate\Routing\Controllers\Controller;
use View, Input, Redirect, Session, Validator, Response, Mail;
use Book as BookModel;
use Company as CompanyModel;
use Cart as CartModel;

class BookController extends \BaseController {
    public function index() {
        $param['books'] = BookModel::where('company_id', Session::get('company_id'))->orderBy('created_at', 'DESC')->paginate(PAGINATION_SIZE);
        $param['pageNo'] = 3;
        if ($alert = Session::get('alert')) {
            $param['alert'] = $alert;
        }
        return View::make('company.book.index')->with($param);        
    }

    public function delete($id) {
        try {
            BookModel::find($id)->delete();
            
            $book->status = Input::get('status');
            $book->save();
            $books = BookModel::find($id);
            $param = [
	            'book_date' => $books->book_date,
	            'duration' => $books->duration,
	            'store_name' => $books->store->name,
	            'user_name' => $books->user->name,
	            'status' => 'deleted',
            ];
            $info = [
	            'reply_name'  => REPLY_NAME,
	            'reply_email' => REPLY_EMAIL,
	            'email'       => $books->user->email,
	            'name'        => $books->user->name,
	            'subject'     => SITE_NAME,
            ];
             
            Mail::send('email.bookStatus', $param, function($message) use ($info) {
            	$message->from($info['reply_email'], $info['reply_name']);
            	$message->to($info['email'], $info['name'])
            	->subject($info['subject']);
            });
            $alert['msg'] = 'Book has been deleted successfully';
            $alert['type'] = 'success';
        } catch(\Exception $ex) {
            $alert['msg'] = 'This Book has been already used';
            $alert['type'] = 'danger';
        }

        return Redirect::route('company.book')->with('alert', $alert);
    }
    
    public function view($id){
    	$param['company'] = CompanyModel::find(Session::get('company_id'));
    	$param['cart'] = CartModel::find($id);
    	return View::make('company.book.view')->with($param);
    }
    
    public function update(){
    	
    	$id = Input::get('bookId');
    	
    	$books = BookModel::find($id);
    	$books->status = Input::get('status');
    	$books->save();
    	
    	$param = [
	    	'book_date' => $books->book_date,
	    	'duration' => $books->duration,
	    	'store_name' => $books->store->name,
	    	'user_name' => $books->user->name,
	    	'status' => 'cancelled',
    	];
    	$info = [
	    	'reply_name'  => REPLY_NAME,
	    	'reply_email' => REPLY_EMAIL,
	    	'email'       => $books->user->email,
	    	'name'        => $books->user->name,
	    	'subject'     => SITE_NAME,
    	];
    	
    	Mail::send('email.bookStatus', $param, function($message) use ($info) {
    		$message->from($info['reply_email'], $info['reply_name']);
    		$message->to($info['email'], $info['name'])
    		->subject($info['subject']);
    	});
    	return Response::json(['result' => 'success', 'msg' => 'Status updated successfully!']);
    }
}
