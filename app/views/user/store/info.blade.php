@if (count($stores) == 0)
    <div class="col-sm-12">
    <h3 class="color-default">There is no similars</h3>
    </div>
@endif
    
@foreach ($stores as $item)
<div class="col-sm-12 margin-top-xs margin-bottom-xs">
    <div style="position: relative;">
        <?php 
            $tooltip = "<h4>".$item->name."</h4>";
            $tooltip.= "<p><b>Keywords : </b>";
            $keywords = explode(",", $item->keyword);
            foreach ($keywords as $subKey => $value) {
                if ($subKey != 0)
                    $tooltip.=", ";
                $tooltip.= $value;
            }
            $tooltip.= "</p>";
            $tooltip.= "<p><b>Email : </b>".$item->company->email."</p>";
            $tooltip.= "<p><b>Phone : </b>".$item->phone."</p>";
            $tooltip.= "<p><b>VAT ID : </b>".$item->company->vat_id."</p>";            
            $tooltip.= "<p><b>Description : </b>".substr($item->description, 0, 300)."</p>";
        ?>
        <a href="{{ URL::route('store.detail', $item->slug) }}">
            <img class="img-responsive img-rounded" style="height: 165px;" src="{{ HTTP_STORE_PATH.$item->photo }}" data-toggle="tooltip" data-placement="right" data-html="true" data-title="{{ $tooltip }}">
        </a>
        <div class="featured-company-offer">
        @if (count($item->company->offers) > 0)
            @foreach ($item->company->offers as $offer)
                @if (!$offer->is_review)
                <div class="color-default"><b>{{ $offer->name." : " }}</b>{{ $offer->price."&euro;" }}</div>
                @endif
            @endforeach                    
        @endif
        </div>
        <div class="similar-item">
            <p style="position: absolute; bottom: 25px; right: 7px; font-size: 14px;">
                <a class="color-white" href="{{ URL::route('store.detail', $item->slug) }}" data-toggle="tooltip" data-placement="right" data-html="true" data-title="{{ $tooltip }}">
                    {{ $item->name }}
                </a>
            </p>
            
            <div class="pull-right">
                <input id="js-number-rating" type="number" class="rating" min=0 max=5 step=1 data-show-clear=false data-show-caption=false data-size='xs' value="{{ $item->getRatingScore() }}" readonly=true style="cursor: pointer;">
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endforeach