<?php namespace Company;

use Illuminate\Routing\Controllers\Controller;
use View, Input, Redirect, Session, Validator, Mail, URL;
use Company as CompanyModel, CompanyDetail as CompanyDetailModel;
use City as CityModel, Category as CategoryModel, CompanySubCategory as CompanySubCategoryModel, SubCategory as SubCategoryModel;
use RatingType as RatingTypeModel;
use Pclass as PclassModel;
use ProfSubClass as ProfSubClassModel;
use SubClass as SubClassModel;
use Plan as PlanModel;
use CompanyTransaction as CompanyTransactionModel;
class AuthController extends \BaseController {

    public function index() {
        if (Session::has('company_id')) {
            return Redirect::route('company.dashboard');
        } else {
            return Redirect::route('company.auth.login');
        }
    }
    
    public function login() {
        if (Session::has('company_id')) {
            return Redirect::route('company.dashboard');
        }
        
        $param['pageNo'] = 51;
        if ($alert = Session::get('alert')) {
            $param['alert'] = $alert;
        }
        return View::make('company.auth.login')->with($param);
    }
    
    public function doLogin() {
        $email = Input::get('email');
        $password = Input::get('password');
        
        $company = CompanyModel::whereRaw('email = ? and secure_key = md5(concat(salt, ?))', array($email, $password))->get();
    
        if (count($company) != 0 > 0) {
            Session::set('company_id', $company[0]->id);
            Session::set('company_name', $company[0]->name);
            Session::set('company_type', $company[0]->user_type);
            Session::set('company_service_amount', $company[0]->service_amount);
            $user = CompanyModel::find($company[0]->id);
            $user->session_status = 1;
            $user->save();
            return Redirect::route('company.dashboard');
        } else {
            $alert['msg'] = 'Invalid username and password';
            $alert['type'] = 'danger';
            return Redirect::route('company.auth.login')->with('alert', $alert);
        }
    }
    
    public function signup($type, $price) {
        if (Session::has('company_id')) {
            return Redirect::route('company.dashboard');
        }

        $param['pageNo'] = 52;
        if ($alert = Session::get('alert')) {
            $param['alert'] = $alert;
        }
        $param['type'] = $type;
        $param['price'] = $price;
        $param['plans'] = Planmodel::All();
        return View::make('company.auth.signup')->with($param);
    }
	
    public function signSuccess() {
    	
    	
    	$alert['msg'] = 'You have registered successfully.';
    	$alert['type'] = 'success';
    
    	return Redirect::route('company.auth.login')->with('alert', $alert);
    }
    
    public function signFailed(){
    	$alert['msg'] = 'You have failed on signup.';
    	$alert['type'] = 'danger';
    	
    	return Redirect::route('company.auth.signup')->with('alert', $alert);
    }
    
    public function membership(){
    	if(Session::has('company_id')){
    		return Redirect::route('company.dashboard');
    	}
    	$param['pageNo'] = 52;
    	if ($alert = Session::get('alert')) {
    		$param['alert'] = $alert;
    	}
    	$param['plans'] = PlanModel::All();
    	return View::make('company.auth.membership')->with($param);
    }
    
    public function doSignup() {
        
        $rules = ['email'      => 'required|email|unique:company',
                  'password'   => 'required|confirmed',
                  'password_confirmation' => 'required',
                  'name'       => 'required',
        ];
        
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()) {
            return Redirect::back()
            ->withErrors($validator)
            ->withInput();
        } else {
            $company = new CompanyModel;
            $company->name = Input::get('name');
            $company->email = Input::get('email');
            $company->phone = Input::get('phone');
            $company->photo = DEFAULT_PHOTO;
            $company->keyword = Input::get('keyword');
            $company->user_type='basic';
            $company->service_amount = 1;            
            $company->count_email = 0;
            $company->count_sms = 0;
            $company->is_completed = FALSE;
            $company->token = strtoupper(str_random(8));
            $company->salt = str_random(8);
            $company->secure_key = md5($company->salt.Input::get('password'));
            $company->save();

            
            
            foreach (['Service', 'Quality', 'Clean'] as $value) {
                $ratingType = new RatingTypeModel;
                $ratingType->company_id = $company->id;
                $ratingType->name = $value;
                $ratingType->save();
            }
            

            $alert['msg'] = 'Please login and complete your profile';
            $alert['type'] = 'success';
        
            return Redirect::route('company.auth.login')->with('alert', $alert);
        }
    }    
    
    public function logout() {
    	$user = CompanyModel::find(Session::get('company_id'));
    	$user->session_status = 0;
    	$user->save();
        Session::forget('company_id');
        Session::forget('company_name');
        return Redirect::route('company.auth.login');
    }
    
    public function profile($id = 1) {
        $param['pageNo'] = 9;
        $param['tabNo'] = $id;
        if ($alert = Session::get('alert')) {
            $param['alert'] = $alert;
        }
        
        $param['company'] = CompanyModel::find(Session::get('company_id'));
        $param['categories'] = PclassModel::all();
        
        return View::make('company.auth.profile')->with($param);
    }

     
    public function changePassword() {
        $rules = ['password_current' => 'required',
                  'password' => 'required|confirmed',
                  'password_confirmation' => 'required',
        ];
        
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()) {
            return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
        } else {
            $company = CompanyModel::find(Session::get('company_id'));
            if ($company->secure_key == md5($company->salt.Input::get('password_current'))) {
                $company->secure_key = md5($company->salt.Input::get('password'));
                $company->save();
                
                $alert['msg'] = 'Password has been updated successfully';
                $alert['type'] = 'success';                                
            } else {
                $alert['msg'] = 'Current Password is incorrect';
                $alert['type'] = 'danger';                
            }
            return Redirect::route('company.profile', 4)->with('alert', $alert);            
        }        
    }
    
    public function updateCompany() {
        $company = CompanyModel::find(Session::get('company_id'));
        $company->name = Input::get('name');
        $company->email = Input::get('email');
        $company->phone = Input::get('phone');
        $company->vat_id = Input::get('vat_id');
        $company->keyword = Input::get('keyword');
        $company->description = Input::get('description');
        $company->paypal_id = Input::get('paypal_id');
        $company->is_completed = TRUE;
        $company->save();
        
        ProfSubClassModel::where('company_id', Session::get('company_id'))->delete();
        if (Input::has('sub_category')) {
            foreach (Input::get('sub_category') as $subCategory) {
                $subCategory = SubClassModel::find($subCategory);
            
                $companySubCategory = new ProfSubClassModel;
                $companySubCategory->company_id = Session::get('company_id');
                $companySubCategory->class_id = $subCategory->class_id;
                $companySubCategory->sub_class_id = $subCategory->id;
                $companySubCategory->save();
            }            
        }
        
        $alert['msg'] = 'Professional has been updated successfully';
        $alert['type'] = 'success';        
        
        return Redirect::route('company.profile', 1)->with('alert', $alert);
    }
    
    public function updatePhoto() {
        $company = CompanyModel::find(Session::get('company_id'));
        if (Input::hasFile('photo')) {
            $filename = str_random(24).".".Input::file('photo')->getClientOriginalExtension();
            Input::file('photo')->move(ABS_COMPANY_PATH, $filename);
            $company->photo = $filename;
        }
        $company->save();        
        
        $alert['msg'] = 'Professional Photo has been updated successfully';
        $alert['type'] = 'success';
        
        return Redirect::route('company.profile', 3)->with('alert', $alert);
    }
    
    public function purchaseIPN() {
    	$req = 'cmd=_notify-validate';  
    	foreach ($_POST as $key => $value) {
    		if ( $key == "custom" ) {
    				
    			$custom = json_decode($value, true);
    			if(!isset($custom['com_id'])){
    				$email = $custom["email"];
    				$name = $custom["name"];
    				$phone = $custom["phone"];
    				$keyword = $custom["keyword"];
    				$password = $custom["password"];
    				
    			}else
    			    $com_id = $custom["com_id"];
    			if(isset($custom["service_number"]))
    				$service_number = $custom["service_number"];
    			
    			$user_type = $custom['type'];
    		}
    			
    		if ( $key == "txn_id" )
    			$txn_id = $value;
    			
    		if ( $key == "mc_gross" )
    			$mc_gross = $value;
    		
    		if( $key == 'receiver_id')
    			$subscr = $value;
    
    		$value = urlencode(stripslashes($value));
    		$req .= "&$key=$value";
    	}

 
    	//post back to PayPal system to validate
    	$header = "POST /cgi-bin/webscr HTTP/1.1\r\n";
    	$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
    	$header .= "Host: ".PAYPAL_SERVER."\r\n";
    	$header .= "Connection: close\r\n";
    	$header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
    	$fp = fsockopen ('ssl://'.PAYPAL_SERVER, 443, $errno, $errstr, 30);
    	if ($fp) {

    		fputs ($fp, $header . $req);
    		while (!feof($fp)) {
    			$res = fgets ($fp, 1024);
    			$res = trim($res);
    
    			if (strcmp($res, "VERIFIED") == 0)
    			{
    				//insert order into database
    				if(isset($com_id)){

    					$company = CompanyModel::find($com_id);
    					$company->user_type = $user_type;
    					if(isset($service_number))
    						$company->service_amount = (int)$service_number + $company->service_amount;

    					$company->save();
    					$companyTransaction = new CompanyTransactionModel;
    					$companyTransaction->company_id = $company->id;
    					$companyTransaction->subscr_id = $subscr;
    					$companyTransaction->txn_id = $txn_id;
    					$companyTransaction->amount = $mc_gross;
    					$companyTransaction->save();
    				}else {
    					$company = new CompanyModel;
    					$company->name = $name;
    					$company->email = $email;
    					$company->phone = $phone;
    					$company->count_email = 0;
    					$company->count_sms = 0;    					
    					$company->keyword = $keyword;
    					$company->user_type = $user_type;
    					$company->photo = DEFAULT_PHOTO;
    					if(isset($service_number))
    						$company->service_amount = $service_number;
    					$company->is_completed = FALSE;
    					$company->token = strtoupper(str_random(8));
    					$company->salt = str_random(8);
    					$company->secure_key = md5($company->salt.$password);
    					if(count(CompanyTransactionModel::where('txn_id', $txn_id)->get()) == 0){
    						$company->save();
    						$companyOpening = new CompanyOpeningModel;
    						$companyOpening->company_id = $company->id;
    						foreach (['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun', ] as $key) {
    							$companyOpening->{ $key.'_start'} = DEFAULT_START_TIME;
    							$companyOpening->{ $key.'_end'} = DEFAULT_END_TIME;
    						}
    						$companyOpening->save();
    							
    						foreach (['Service', 'Quality', 'Clean'] as $value) {
    							$ratingType = new RatingTypeModel;
    							$ratingType->company_id = $company->id;
    							$ratingType->name = $value;
    							$ratingType->save();
    						}
    							
    						$companyTransaction = new CompanyTransactionModel;
    						$companyTransaction->company_id = $company->id;
    						$companyTransaction->subscr_id = $subscr;
    						$companyTransaction->txn_id = $txn_id;
    						$companyTransaction->amount = $mc_gross;
 
    						$companyTransaction->save();
    							
    						$param = [
    						'company_name' => $company->name,
    						'link' => URL::route('company.auth.login')
    						];
    						$info = [
    						'reply_name'  => REPLY_NAME,
    						'reply_email' => REPLY_EMAIL,
    						'email'       => $company->email,
    						'name'        => $company->name,
    						'subject'     => SITE_NAME,
    						];
    					
    						Mail::send('email.comReg', $param, function($message) use ($info) {
    							$message->from($info['reply_email'], $info['reply_name']);
    							$message->to($info['email'], $info['name'])
    							->subject($info['subject']);
    						});
    						// \End send mail to customer
    					}
    				}	
    				
    			}
    			if (strcmp ($res, "INVALID") == 0) {
    
    			}
    		}
    		fclose($fp);
    	}
    }
}