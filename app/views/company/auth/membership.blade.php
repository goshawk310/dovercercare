@extends('company.layout')

@section('custom-styles')
    <style>
    .control-label {
        font-weight: bold;
    }
    .page-container {
        background: #FFF;
    }
    </style>
@stop

@section('main')
<div class="page-container">
    <div class="page-contect-wrapper">
	    <div class="page-content"> 
            <div class="col-sm-8 col-sm-offset-2 padding-bottom-lg" style="margin-top:120px;">             
                <div class="col-md-3 col-sm-offset-2" style="margin-top:95px;">
                  <div class="pricing hover-effect">
                    <div class="pricing-head">
                      <h3>Professional Basic
                      <span>
                         Officia deserunt mollitia
                      </span>
                      </h3>
                      <h4>Free</h4>
                    </div>
                    <ul class="pricing-content list-unstyled">
                      <li>
                        <i class="fa fa-tags"></i>VIEWING YOUR PROFESSIONAL DETAILS
                      </li>
                      <li>
                        <i class="fa fa-asterisk"></i>PERSONAL ADMINISTRATOR PANEL
                      </li>
                      <li>
                        <i class="fa fa-heart"></i>ONE SERVICE AND ONE OFFICE
                      </li>
                      <li>
                        <i class="fa fa-star"></i>PERSONAL CONNECTION URL
                      </li>
                      <li>
                        <i class="fa fa-shopping-cart"></i>INDEX PROFILE
                      </li>
                    </ul>
                    <div class="pricing-footer">
                      <p>
                         Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
                      </p>
                      <a href="{{ URL::route('company.auth.signup', array('basic','free') ) }}" class="btn btn-primary">
                         Sign Up <i class="m-icon-swapright m-icon-white"></i>
                      </a>
                    </div>
                  </div>
                </div>
                @foreach($plans as $key => $plan)
                <div class="col-md-3">
                  <div class="pricing pricing-active hover-effect">
                    <div class="pricing-head pricing-head-active">
                      <h3>{{$plan->name}}
                      <span>
                         Officia deserunt mollitia
                      </span>
                      </h3>
                      <h4><i>&euro;</i>{{$plan->price}}</i>
                      <span>
                         {{$plan->type == 'py' ? 'Per Year' : 'Per Service'}}
                      </span>
                      </h4>
                    </div>
                    <ul class="pricing-content list-unstyled">
                      <li>
                        <i class="fa fa-tags"></i>VIEWING YOUR PROFESSIONAL DETAILS
                      </li>
                      <li>
                        <i class="fa fa-asterisk"></i>PERSONAL ADMINISTRATOR PANEL
                      </li>
                      <li>
                        <i class="fa fa-heart"></i>MULTIPLE SERVICES AND MULTIPLE OFFICES
                      </li>
                      <li>
                        <i class="fa fa-star"></i>PERSONAL CONNECTION URL
                      </li>
                      <li>
                        <i class="fa fa-shopping-cart"></i>INDEX PROFILE
                      </li>
                      <li>
                        <i class="fa fa-tree"></i>OFFERS PUBLICATION
                      </li>
                      <li>
                        <i class="fa fa-child"></i>SECTION PERSONAL ITEMS
                      </li>
                    </ul>
                    <div class="pricing-footer">
                      <p>
                         Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
                      </p>
                      <a href="{{ URL::route('company.auth.signup', array('pro_'.$plan->type, $plan->price ) ) }}" class="btn btn-primary">
                         Sign Up <i class="m-icon-swapright m-icon-white"></i>
                      </a>
                    </div>
                  </div>
                </div>
			@endforeach                
            </div>
        </div>
    </div>
</div>
@stop

@stop
