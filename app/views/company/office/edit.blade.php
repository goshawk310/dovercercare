@extends('company.layout')

@section('custom-styles')]
    <style>
      #map-canvas {
        height: 300px;
      }
    </style>
    {{ HTML::style('/assets/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}
   
@stop

@section('breadcrumb')
	<div class="row">
		<div class="col-md-12">
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<span>Office</span>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<span>Edit</span>
				</li>
			</ul>
			
		</div>
	</div>    
@stop

@section('content')

@if ($errors->has())
<div class="alert alert-danger alert-dismissibl fade in">
    <button type="button" class="close" data-dismiss="alert">
        <span aria-hidden="true">&times;</span>
        <span class="sr-only">Close</span>
    </button>
    @foreach ($errors->all() as $error)
		{{ $error }}		
	@endforeach
</div>
@endif

<div class="portlet box red">
    <div class="portlet-title">
		<div class="caption">
			<i class="fa fa-pencil-square-o"></i> Edit Office
		</div>
	</div>
	<div class="portlet-body form">
        <form class="form-horizontal form-bordered form-row-stripped" role="form" method="post" action="{{ URL::route('company.office.store') }}" enctype="multipart/form-data">
            <input type="hidden" name="office_id" value="{{ $office->id }}"/>            
            <div class="form-body">
                @foreach ([
                    'name' => 'Name',                           
                    'address' => 'Address',
                    'opening_hours'=>'Opening Hours'
                ] as $key => $value)
                @if($key == 'address')
                <div class="form-group">
                    <label class="col-sm-3 control-label">{{ Form::label($key, $value) }}</label>
                    <div class="col-md-7">                    
                        <input type="text" class="form-control" name="{{ $key }}" value="{{ $office->{$key} }}">                      
                    </div>
                    <div class="col-sm-2">
                    	<button type="button" id="addrMarker" class="btn btn-default">
                        <span class="glyphicon glyphicon-map-marker"></span> Find Address
                    </button>
                    </div>
                </div>
                <!-- Opening setting imak 20150610 -->
                @elseif ($key == 'opening_hours')
                <div class="form-group">
                    <label class="col-sm-3 control-label">{{ Form::label($key, $value) }}</label>
                    <div class="col-sm-9" style="padding:0px;">
                    	@foreach ([
                                        'mon' => 'Mon',
                                        'tue' => 'Tue',
                                        'wed' => 'Wed',
                                        'thu' => 'Thu',
                                        'fri' => 'Fri',
                                        'sat' => 'Sat',
                                        'sun' => 'Sun',
                                    ] as $k => $v)
                               <div class="form-group">
                               		<label class="control-label col-md-3">{{ Form::label($k, $v).' : ' }}</label>
                               		<div class="col-md-4">
											<input type="text" placeholder="Start Time" class="form-control" name="{{ $k }}_start" value="{{ $office->opening->{$k.'_start'} }}">
										</div>
										<div class="col-md-4">
											<input type="text" placeholder="End Time" class="form-control" name="{{ $k }}_end" value="{{ $office->opening->{$k.'_end'} }}">
										</div>
                               </div>
                               @endforeach
                               </div>
                           </div> 
                      <!-- / Opening setting imak 20150610 -->
                @else                
                <div class="form-group">
                    <label class="col-sm-3 control-label">{{ Form::label($key, $value) }}</label>
                    <div class="col-md-9">                    
                        <input type="text" class="form-control" name="{{ $key }}" value="{{ $office->{$key} }}">                      
                    </div>                    
                </div>
                @endif
                @endforeach
				<div class="form-group">
					<label class="control-label col-md-3">Location</label>
					<div class="col-md-9">
					    <div id="map-canvas"></div>
					</div>
					<input type="hidden" name="lat" value="{{ $office->lat }}"/>
					<input type="hidden" name="lng" value="{{ $office->lng}}"/>
				</div>                
            </div>
            <div class="form-actions fluid">
                <div class="col-sm-12 text-center">
                    <button type="submit" class="btn btn-success">
                        <span class="glyphicon glyphicon-ok-circle"></span> Save
                    </button>
                    <a href="{{ URL::route('company.office') }}" class="btn btn-primary">
                        <span class="glyphicon glyphicon-share-alt"></span> Back
                    </a>
                </div>
            </div>            
        </form>
    </div>
</div>
@stop

@section('custom-scripts')
{{ HTML::script('/assets/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>

<script>
var map;
var marker;
var lat,lng;
$(document).ready(function() {	
	lat = $("input[name='lat']").val();
	lng = $("input[name='lng']").val();
	markerAddress(lat,lng);
	google.maps.event.addListener(map, 'click', function(event) {
        marker.setPosition(event.latLng);
        $("input[name='lat']").val(event.latLng.lat());
        $("input[name='lng']").val(event.latLng.lng());
    });
});

jQuery('#addrMarker').click(function(){
	var address = jQuery('input[name="address"]').val();
	var geocoder = new google.maps.Geocoder();

	geocoder.geocode( { 'address': address}, function(results, status) {

	if (status == google.maps.GeocoderStatus.OK) {
	    var latitude = results[0].geometry.location.lat();
	    var longitude = results[0].geometry.location.lng();
	    markerAddress(latitude,longitude);console.log(latitude);
	    }
	else {
		bootbox.alert('Please input your address correctly!');
        window.setTimeout(function(){
            bootbox.hideAll();
        }, 2000);
		}
	});
});

function markerAddress(lat,lng){
	var myLatlng = new google.maps.LatLng(lat,lng);
    var mapOptions = {
      zoom: 10,
      center: myLatlng
    }
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: 'Office Location'
    });
    $("input[name='lat']").val(lat);
    $("input[name='lng']").val(lng);
    
}
</script>

@stop

@stop
