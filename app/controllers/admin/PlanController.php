<?php namespace Admin;

use Illuminate\Routing\Controllers\Controller;
use View, Input, Redirect, Session, Validator;
use Plan as PlanModel;

class PlanController extends \BaseController {
    
    public function index() {
        $param['plans'] = PlanModel::paginate(PAGINATION_SIZE);
        $param['pageNo'] = 11;
        if ($alert = Session::get('alert')) {
            $param['alert'] = $alert;
        }
        
        return View::make('admin.plan.index')->with($param);
    }
    
    public function create() {
        $param['pageNo'] = 11;
        return View::make('admin.plan.create')->with($param);
    }
    
    public function edit($id) {
        $param['pageNo'] = 11;
        $param['plan'] = PlanModel::find($id);
        
        return View::make('admin.plan.edit')->with($param);
    }
    
    public function store() {
        
        $rules = ['name' => 'required'];
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        } else {
            if (Input::has('plan_id')) {
                $id = Input::get('plan_id');
                $plan = PlanModel::find($id);
            } else {
                $plan = new PlanModel;                
            }            
            $plan->name = Input::get('name');
            $plan->price = Input::get('price');
            $plan->type = Input::get('type');            
            $plan->code = Input::get('code');
            $plan->save();
            
            $alert['msg'] = 'Plan has been saved successfully';
            $alert['type'] = 'success';            
              
            return Redirect::route('admin.plan')->with('alert', $alert);            
        }
    }
    
    public function delete($id) {
        try {
            PlanModel::find($id)->delete();
            
            $alert['msg'] = 'Plan has been deleted successfully';
            $alert['type'] = 'success';            
        } catch(\Exception $ex) {
            $alert['msg'] = 'This Plan has been already used';
            $alert['type'] = 'danger';
        }

        return Redirect::route('admin.plan')->with('alert', $alert);
    }
}
