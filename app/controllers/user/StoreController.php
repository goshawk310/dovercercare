<?php namespace User;

use Illuminate\Routing\Controllers\Controller;
use View, Input, Redirect, Session, DB, Response, Mail;
use Category as CategoryModel, City as CityModel;
use Feedback as FeedbackModel, ReviewPhoto as ReviewPhotoModel;
use Store as StoreModel;
use Company as CompanyModel;
use Consumer as ConsumerModel;
use CommonFunction as CommonFunctionModel;
use Cart as CartModel, Chat as ChatModel;
use Office as OfficeModel;
use Stoffice as StofficeModel;
use Pclass as PclassModel;
use ProfSubClass as ProfSubClassModel;

class StoreController extends \BaseController {
    
    public function home() {
        $param['categories'] = CategoryModel::all();
        $param['classes'] = PclassModel::all();
        $param['officies'] = OfficeModel::all();
    
        if ($alert = Session::get('alert')) {
            $param['alert'] = $alert;
        }
        if (!Session::has('keyword')) {
            Session::set('keyword', '');
            Session::set('location', '');
        }
    
        // $param['stores'] = StoreModel::completed()->orderBy(DB::raw('RAND()'))->take(12)->get();
        $param['stores'] = StoreModel::orderBy(DB::raw('RAND()'))->take(12)->get();
        $param['feedbacks'] = FeedbackModel::orderBy('id', 'DESC')->take(8)->get();
        
        return View::make('user.store.home')->with($param);
    }    
    
    public function search() {
        $keyword = Input::has('keyword') ? Input::get('keyword') : '';
        $location = Input::has('location') ? Input::get('location') : '';
        $lat = Input::has('lat') ? Input::get('lat') : '';
        $lng = Input::has('lng') ? Input::get('lng') : '';
        
        if ($lat == '' && $lng == '') {
            $ipAddr = $_SERVER['REMOTE_ADDR'];
            $geoPlugin = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ipAddr));
            
            if (is_numeric($geoPlugin['geoplugin_latitude']) && is_numeric($geoPlugin['geoplugin_longitude'])) {            
                $lat = $geoPlugin['geoplugin_latitude'];
                $lng = $geoPlugin['geoplugin_longitude'];
            }
        }      
        if (Input::has('dt')) {
            Session::set('dt', Input::get('dt'));
        } elseif (!Session::has('dt')) {
            Session::set('dt', 'list');
        }
    
        // $result = StoreModel::completed()->search($keyword, $location);
        $result = StoreModel::search($keyword, $location);
        $tblStore =with(new StoreModel)->getTable();
        $param['categories'] = CategoryModel::all();
        $param['classes'] = PclassModel::all();
        $param['officies'] = OfficeModel::all();
        if (Session::get('dt') == 'grid'){
        	$param['stores'] = $result->groupBy($tblStore.'.id')->orderBy('distance', 'ASC')->paginate(PAGINATION_SIZE * 4);        	
        }            
        else {
        	$param['stores'] = $result->groupBy($tblStore.'.id')->orderBy('distance', 'ASC')->paginate(5);        	
        } 
            
    
        Session::set('keyword', $keyword);
        Session::set('location', $location);
    
        return View::make('user.store.search')->with($param);
    }    
    
    public function profSearch(){
    	if (Input::has('dt')) {
    		Session::set('dt', Input::get('dt'));
    	} elseif (!Session::has('dt')) {
    		Session::set('dt', 'list');
    	}
    	$classes = Input::has('classes') ? Input::get('classes') : '';
    	$location = Input::has('location') ? Input::get('location') : '';
    	$keyword = '';
    	$classes = explode(',',$classes);
    	Session::set('classes', $classes);
    	$param['classes'] = PclassModel::all();
    	$param['categories'] = CategoryModel::all();
    	$param['officies'] = OfficeModel::all();
    	
    	$result = CompanyModel::Profsearch($classes, $keyword, $location);
    	$id = [];
    	foreach($result->get() as $p){
    		$id[] = $p->id;
    	}
    	$result = CompanyModel::whereIn('id',$id);
    	if (Session::get('dt') == 'grid'){    		
    			$param['companies'] = $result->paginate(PAGINATION_SIZE * 4);    			
    	}
    	else {    			
    			$param['companies'] = $result->paginate(5);
    	}    	
    	
    	Session::set('location', $location);
    	return View::make('user.store.profsearch')->with($param);
    }
    
    public function detailProfile($slug) {
        $store = StoreModel::findBySlug($slug);
        $param['stores'] = StoreModel::where('company_id', $store->company_id)->where('id', '<>', $store->id)->get();
        $param['prof_id'] = $store->company_id;
        $param['prof_name'] = $store->company->name;
        $param['store'] = $store;
        $param['personal_officies'] = StofficeModel::where('store_id',$store->id)->get();
        $store->count_view = $store->count_view + 1;
        $store->save();
        $param['categories'] = CategoryModel::all();
        $param['classes'] = PclassModel::all();
        $param['officies'] = OfficeModel::all();
        $param['subPageNo'] = 1;
        if ($alert = Session::get('alert')) {
            $param['alert'] = $alert;
        }
        
        if (Session::has('user_id')) {
            if (FeedbackModel::where('user_id', Session::get('user_id'))
                            ->where('store_id', $param['store']->id)
                            ->where('status', '<>', 'ST03')
                            ->get()
                            ->count() > 0) {
                $param['is_valid'] = FALSE;
            } else {
                $param['is_valid'] = TRUE;
            }
        } else {
            $param['is_valid'] = FALSE;
        }        
        
        return View::make('user.store.detailProfile')->with($param);
    }
    
    
    public function detailPhoto($slug) {
        $param['store'] = StoreModel::findBySlug($slug);
        $param['categories'] = CategoryModel::all();
        $param['cities'] = CityModel::all();
        $param['subPageNo'] = 2;
        if ($alert = Session::get('alert')) {
            $param['alert'] = $alert;
        }
        if (Session::has('user_id')) {
            if (ReviewPhotoModel::where('user_id', Session::get('user_id'))
                            ->where('store_id', $param['store']->id)
                            ->get()
                            ->count() > 0) {
                $param['is_valid'] = FALSE;
            } else {
                $param['is_valid'] = TRUE;
            }
        } else {
            $param['is_valid'] = FALSE;
        }        
        return View::make('user.store.detailPhoto')->with($param);
    }
    
    public function asyncJoin() {
        if (Session::has('user_id')) {
            $companyId = Input::get('company_id');
            $userId = Session::get('user_id');
            $consumer = ConsumerModel::where('company_id', $companyId)->where('user_id', $userId)->get();
            if ($consumer->count() > 0) {
                $result['result'] = "failed";
                $result['msg'] = "You have already joined on this store.";                
            } else {
                CommonFunctionModel::addConsumer($companyId, $userId, 0);
                $result['result'] = "success";
                $result['msg'] = "You have successfully joined on this store.";                
            }
        } else {
            $result['result'] = "failed";
            $result['msg'] = "You have to login";            
        }

        return Response::json($result, 200);
    }
    
    public function chatsave(){
    	if(Input::get('direction') == 'up'){
    		$from = 'u'.Input::get('from');
    		$to = 'p'.Input::get('to');
    	}else{
    		$from = 'p'.Input::get('from');
    		$to = 'u'.Input::get('to');
    	}
    		
    	$message = Input::get('message');
    	$sent = date("Y-m-d H:i:s");
    	$chat = new ChatModel;
    	$chat->from = $from;
    	$chat->to = $to;
    	$chat->message = $message;
    	$chat->save();
    	$result['result'] = 'success';
    	return Response::json($result, 200);
    }
    
    public function asyncBookTime(){
    	$ct = Input::get('ct');
    	$store_id = Input::get('storeId');
    	$result = CartModel::where('store_id',$store_id)->where('book_date','like', $ct.'%')->get();
    	return Response::json($result, 200);
    }
    
    public function purchaseSuccess($slug) {
    	$alert['msg'] = 'You have purchased successfully.';
    	$alert['type'] = 'success';
    
    	return Redirect::route('store.detail', $slug)->with('alert', $alert);
    }
    
    public function purchaseFailed($slug) {
    	$alert['msg'] = 'You have failed on purchasing.';
    	$alert['type'] = 'danger';
    
    	return Redirect::route('store.detail', $slug)->with('alert', $alert);
    }
    
    public function purchaseIPN() {
    	$req = 'cmd=_notify-validate';
    
    	foreach ($_POST as $key => $value) {
            
			if ( $key == "custom" ) {
					
				$custom = json_decode($value, true);
				
				$user_id = $custom["uid"];
				$company_id = $custom["cid"];
				$store_id = $custom["sid"];
				$book_date = $custom["book_date"];
				$duration = $custom["duration"];
				$addr = $custom["addr"];
				$msg = $custom["msg"];
				$office_id = $custom['office_id'];				
			}
			
			if ( $key == "txn_id" ) 
				$txn_id = $value;
			
			if ( $key == "mc_gross" ) 
				$mc_gross = $value;
				
				
			$value = urlencode(stripslashes($value));
    		$req .= "&$key=$value";
    	}

       
    	//post back to PayPal system to validate
    	$header = "POST /cgi-bin/webscr HTTP/1.1\r\n";
    	$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
    	$header .= "Host: ".PAYPAL_SERVER."\r\n";
    	$header .= "Connection: close\r\n";
    	$header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
    	$fp = fsockopen ('ssl://'.PAYPAL_SERVER, 443, $errno, $errstr, 30);
    	if ($fp) {
    		fputs ($fp, $header . $req);
    		while (!feof($fp)) {
    			$res = fgets ($fp, 1024);
    			$res = trim($res);
    
    			if (strcmp($res, "VERIFIED") == 0)
    			{
    				//insert order into database
    				 
    				$cart = new CartModel;
    				$cart->user_id = $user_id;
    				$cart->company_id = $company_id;
    				$cart->store_id = $store_id;
    				$cart->txn_id = $txn_id;
    				$cart->price = $mc_gross;
    				$cart->book_date = $book_date;
    				$cart->duration = $duration;
    				$cart->addr = $addr;
    				$cart->msg = $msg;
    				$cart->office_id = $office_id;
    				$cart->status = 3;
    				if(count(CartModel::where('txn_id', $txn_id)->get()) == 0){
    					$cart->save();
    					if(!empty($addr)){
    						$location = 'Home';
    						$addr = $addr;
    					}else{
    						$location = $cart->office->name;
    						$addr = $cart->office->address;
    					}
    					$books = CartModel::where('store_id', $store_id)->where('user_id', Session::get('user_id'))->firstOrFail();
    					$param = ['book_date' => $book_date,
    					'duration' => $duration,
    					'store_name' => $books->store->name,
    					'user_name' => $books->user->name,
    					'price'=>$books->price,
    					'msg' => $msg,
    					'addr' => $addr,
    					'location' => $location,
    					];
    					$info = [ 'reply_name'  => REPLY_NAME,
    					'reply_email' => REPLY_EMAIL,
    					'email'       => $books->user->email,
    					'name'        => $books->user->name,
    					'subject'     => SITE_NAME,
    					];
    					
    					Mail::send('email.makeBook', $param, function($message) use ($info) {
    						$message->from($info['reply_email'], $info['reply_name']);
    						$message->to($info['email'], $info['name'])
    						->subject($info['subject']);
    					});
    					// \End send mail to customer
    					// Send mail to Store.
    					$info = [ 'reply_name'  => REPLY_NAME,
    					'reply_email' => REPLY_EMAIL,
    					'email'       => $books->store->company->email,
    					'name'        => $books->store->name,
    					'subject'     => SITE_NAME,
    					];
    					Mail::send('email.bookNoti', $param, function($message) use ($info) {
    						$message->from($info['reply_email'], $info['reply_name']);
    						$message->to($info['email'], $info['name'])
    						->subject($info['subject']);
    					});
    					// \ End Send mail to store    					
    				}
    			}
    
    			if (strcmp ($res, "INVALID") == 0) {
    
    			}
    		}
    		fclose($fp);
    	}
    }
}
