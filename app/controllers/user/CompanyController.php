<?php namespace User;

use Illuminate\Routing\Controllers\Controller;
use View, Input, Redirect, Session, DB, Response;
use Category as CategoryModel, City as CityModel;
use Feedback as FeedbackModel, ReviewPhoto as ReviewPhotoModel;
use Company as CompanyModel;
use Consumer as ConsumerModel;
use Store as StoreModel;
use CommonFunction as CommonFunctionModel;
use Pclass as PclassModel;
use Office as OfficeModel;

class CompanyController extends \BaseController {
    
    public function home() {
        $param['categories'] = CategoryModel::all();
        $param['officies'] = OfficeModel::all();
        $param['classes'] = PclassModel::all();
        if ($alert = Session::get('alert')) {
            $param['alert'] = $alert;
        }
        if (!Session::has('keyword')) {
            Session::set('keyword', '');
            Session::set('location', '');
        }
    
        //$param['stores'] = StoreModel::completed()->orderBy(DB::raw('RAND()'))->take(12)->get();
        $param['companies'] = CompanyModel::orderBy(DB::raw('RAND()'))->take(12)->get();
        $param['stores'] = StoreModel::orderBy(DB::raw('RAND()'))->take(12)->get();
        $param['feedbacks'] = FeedbackModel::orderBy('id', 'DESC')->take(8)->get();
        
        return View::make('user.company.home')->with($param);
    }    
    
    public function search() {
        $keyword = Input::has('keyword') ? Input::get('keyword') : '';
        $location = Input::has('location') ? Input::get('location') : '';
        $lat = Input::has('lat') ? Input::get('lat') : '';
        $lng = Input::has('lng') ? Input::get('lng') : '';
        
        if ($lat == '' && $lng == '') {
            $ipAddr = $_SERVER['REMOTE_ADDR'];
            $geoPlugin = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ipAddr));
            
            if (is_numeric($geoPlugin['geoplugin_latitude']) && is_numeric($geoPlugin['geoplugin_longitude'])) {            
                $lat = $geoPlugin['geoplugin_latitude'];
                $lng = $geoPlugin['geoplugin_longitude'];
            }
        }
        
        if (Input::has('dt')) {
            Session::set('dt', Input::get('dt'));
        } elseif (!Session::has('dt')) {
            Session::set('dt', 'list');
        }
    
        // $result = StoreModel::completed()->search($keyword, $location);
        $result = StoreModel::search($keyword, $location);
    
        $tblStore =with(new StoreModel)->getTable();
    
        $param['categories'] = CategoryModel::all();
        $param['officies'] = OfficeModel::all();
        if (Session::get('dt') == 'grid')
            $param['stores'] = $result->groupBy($tblStore.'.id')->orderBy('distance', 'ASC')->paginate(PAGINATION_SIZE * 4);
        else 
            $param['stores'] = $result->groupBy($tblStore.'.id')->orderBy('distance', 'ASC')->paginate(PAGINATION_SIZE);
    
        Session::set('keyword', $keyword);
        Session::set('location', $location);
    
        return View::make('user.store.search')->with($param);
    }    
    
    public function detailProfile($slug) {
    	$param['categories'] = CategoryModel::all();
    	$param['officies'] = OfficeModel::all();
    	$param['classes'] = PclassModel::all();
    	$company = CompanyModel::findBySlug($slug);
    	$param['company'] = CompanyModel::find($company->id);
    	$param['prof_id'] = $company->id;
    	$param['prof_name'] = $company->name;
    	$param['featured_companies'] = CompanyModel::orderBy(DB::raw('RAND()'))->take(12)->get();
        $param['stores'] = StoreModel::where('company_id', $company->id)->orderBy(DB::raw('RAND()'))->take(12)->get();
        $param['feedbacks'] = FeedbackModel::orderBy('id', 'DESC')->take(8)->get();
        
        return View::make('user.company.detailProfile')->with($param);
    }
    
    
    public function detailPhoto($slug) {
        $param['store'] = StoreModel::findBySlug($slug);
        $param['categories'] = CategoryModel::all();
        $param['classes'] = PclassModel::all();
        $param['officies'] = OfficeModel::all();
        $param['subPageNo'] = 2;
        if ($alert = Session::get('alert')) {
            $param['alert'] = $alert;
        }
        if (Session::has('user_id')) {
            if (ReviewPhotoModel::where('user_id', Session::get('user_id'))
                            ->where('store_id', $param['store']->id)
                            ->get()
                            ->count() > 0) {
                $param['is_valid'] = FALSE;
            } else {
                $param['is_valid'] = TRUE;
            }
        } else {
            $param['is_valid'] = FALSE;
        }        
        return View::make('user.store.detailPhoto')->with($param);
    }
    
    public function asyncJoin() {
        if (Session::has('user_id')) {
            $companyId = Input::get('company_id');
            $userId = Session::get('user_id');
            $consumer = ConsumerModel::where('company_id', $companyId)->where('user_id', $userId)->get();
            if ($consumer->count() > 0) {
                $result['result'] = "failed";
                $result['msg'] = "You have already joined on this store.";                
            } else {
                CommonFunctionModel::addConsumer($companyId, $userId, 0);
                $result['result'] = "success";
                $result['msg'] = "You have successfully joined on this store.";                
            }
        } else {
            $result['result'] = "failed";
            $result['msg'] = "You have to login";            
        }

        return Response::json($result, 200);
    }    
}
