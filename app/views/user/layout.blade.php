@extends('main')    
    @section('page-styles')
        {{ HTML::style('/assets/metronic/global/plugins/typeahead/typeahead.css') }}
        {{ HTML::style('//fonts.googleapis.com/css?family=Bitter') }}
        {{ HTML::style('/assets/metronic/frontend/layout/css/style.css') }}
        {{ HTML::style('/assets/metronic/frontend/layout/css/style-responsive.css') }}
        {{ HTML::style('/assets/metronic/frontend/layout/css/themes/red.css') }}
        {{ HTML::style('/assets/metronic/frontend/layout/css/custom.css') }}
        {{ HTML::style('/assets/metronic/admin/layout/css/layout.css') }}
        {{ HTML::style('/assets/css/style_frontend.css') }}
        {{ HTML::style('/assets/css/messenger.css') }}
    @stop

    @section('body')
        <body class="corporate" >
            @section('header')
            <?php
                if (!isset($pageNo)) {
                    $pageNo = 0;
                }                
            ?>
            <!--  live chat --> 
            @if(Session::has('user_id') && isset($prof_id))
			<div class="messenger bg-white  open">
			    <div class="chat-header text-white bg-gray-dark">
			    	<div class="titlebar clearfix">
			    		<div class="top-lfloat">
			    			<div class="usr-status isOnline"></div>
			    			<div class="usr-name">
			    				<strong>{{$prof_name}}</strong>
			    			</div>
			    		</div>
			    		<div class="open-chat-target open-close-chat"></div>
			    	</div>
			        
			        <a href="#" id="chat-toggle" class="pull-right chat-toggle">
			            <span class="glyphicon glyphicon-chevron-down"></span>
			        </a>
			    </div>
			    <div class="messenger-body" >
			        <ul class="chat-messages" id="chat-log">
			 
			        </ul>
			        <div class="chat-footer">
			            <div class="p-lr-10">
			                <textarea id="chat-message"
			                    class="input-light brad chat-search" placeholder="Your message..."></textarea>
			            </div>
			        </div>
			    </div>
			</div>
			
			@endif
<!-- / End live chat -->
            <div class="pre-header page-header navbar" style="background:#0165BD;">
		        <div class="container">
		            <div class="row">
		                <!-- BEGIN TOP BAR LEFT PART -->
		                <div class="col-sm-3 text-center" style="margin-top:3px;">
                            <a href="http://professional.loc">
                                <img src="/assets/img/logo_company.png" alt="Finternet-Group" style="height: 38px; ">
                            </a>
                        </div>
		                <div class="col-md-4 col-sm-4 additional-shop-info color-white" style="padding-top:15px;">
		                    <ul class="list-unstyled list-inline">
		                        <li><i class="fa fa-phone"></i><span>+1 456 6717</span></li>
		                        <li><i class="fa fa-envelope-o"></i><span>gaetano.gio@gmail.com</span></li>
		                    </ul>
		                </div>
		                <!-- END TOP BAR LEFT PART -->
		                <!-- BEGIN TOP BAR MENU -->
		                <div class="col-md-3 col-sm-3 color-white text-right" style="padding-top:15px;padding-right:0px;">
		                    @if (Session::has('user_id') )
                            <a href="{{ URL::route('user.cart') }}" class="btn-menu {{ $pageNo == 1 ? 'active' : '' }}">Cart</a>
                            |
                            <a href="{{ URL::route('user.profile') }}" class="btn-menu {{ $pageNo == 2 ? 'active' : '' }}">Profile</a>
                            |
                            <a href="{{ URL::route('user.offers') }}" class="btn-menu {{ $pageNo == 3 ? 'active' : '' }}">Offers</a>
                            |
                            <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="cursor:pointer;">Messages</a>
								<ul class="dropdown-menu" style="width:275px;right:0px;top:47px;">
									<li style="  border-bottom: 1px solid #ecd;background:#eaedf2;">
										<p style="color:#333;padding-top:10px;">
											 Messages
										</p>
									</li>
									<li style="padding-left:0px; padding-right:0px;">
										<ul class="dropdown-menu-list scroller" style="height:250px;padding-right:0px;">
											<li style="padding-left:0px; padding-right:0px;   border-bottom: 1px solid #ddd;">
												<a href="inbox.html?a=view">
												<span class="photo">
												<img src="{{ HTTP_USER_PATH. DEFAULT_PHOTO }}" class="img-circle" alt=""/>
												</span>
												<span class="subject">
												<span class="from">
												Lisa Wong </span>												
												<span class="message" style="margin-left:46px; white-space:normal;">
												Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... </span>
												</a>
											</li>											
										</ul>
									</li>									
								</ul>
                            |                            
                            <a href="{{ URL::route('user.doSignout') }}" class="btn-menu">Sign Out</a>
                            @else
                            <a href="{{ isset($redirect) && ($redirect != '') ? URL::route('user.login').'?redirect='.urlencode($redirect) : URL::route('user.login') }}" class="btn-menu {{ $pageNo == 51 ? 'active' : '' }}">Log In</a>
                            |
                            <a href="{{ isset($redirect) && ($redirect != '') ? URL::route('user.signup').'?redirect='.urlencode($redirect) : URL::route('user.signup') }}" class="btn-menu {{ $pageNo == 52 ? 'active' : '' }}">Sign Up</a>                            
                            |
                            <a href="{{ URL::route('company.auth') }}" class="btn-menu">&nbsp;<i class="fa fa-building"></i>&nbsp;For Business</a>
                            @endif
		                </div>
		                <div class="top-menu col-sm-2">
					        <ul class="nav navbar-nav pull-right">
					        	<li class="dropdown dropdown-quick-sidebar-toggler">
									<a href="javascript:;" class="dropdown-toggle">
									<i class="icon-logout"></i> Search
									</a>
								</li>
					        </ul>
					        </div>
		                <!-- END TOP BAR MENU -->
		            </div>
		        </div>
		        
		    </div>            
            <div class="header background-default" style="border-top: 5px solid #AE3030;">                
                <div class="header-navigation font-transform-inherit search-navigation">
                    <ul class="text-center">
                        @foreach ($categories as $key => $value)
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="#" style="color: #FFF;">
                                <i class="{{ $value->icon }}"></i>
                                <b>{{ $value->name }}</b> <span class="caret"></span>
                            </a>
                            
                            <ul class="dropdown-menu">
                                @foreach ($value->subCategories as $subKey => $subValue)
                                <li><a href="{{ URL::route('store.search').'?keyword='.$subValue->name }}">{{ $subValue->name }}</a></li>
                                @endforeach
                            </ul>
                        </li>
                        @endforeach
                    </ul>
                </div>    
                <div class="clearfix"></div>                
            </div>            
            
            <!-- Sidebar Toggler -->
            <a href="javascript:;" class="page-quick-sidebar-toggler"><i class="icon-close"></i></a>
            <div class="page-quick-sidebar-wrapper searchbar">
					<div class="page-quick-sidebar">
						<div class="nav-justified">
							<ul class="nav nav-tabs nav-justified">
								<li class="active">
									<a href="#quick_sidebar_tab_1" data-toggle="tab">
									Professional
									</a>
								</li>
								<li>
									<a href="#quick_sidebar_tab_2" data-toggle="tab">
									Service
									</a>
								</li>								
							</ul>
							<div class="tab-content">
								<div class="tab-pane active page-quick-sidebar-chat" id="quick_sidebar_tab_1">
									<div class="page-quick-sidebar-chat-users" data-rail-color="#ddd" data-wrapper-class="page-quick-sidebar-list">
										<h3 class="list-heading">Search Professional</h3>
										<div class="col-sm-12" style="margin-top:30px;">
		                                    <dl class="dropdown form-group">
											   <dt>
											      <a href="#">
											         <span class="hida">Select Professional</span>
											         <p class="multiSel"></p>
											      </a>
											   </dt>
											   <dd>
											      <div class="res-search-select-wrap res-search-select-wrap-resources multiSelect">
											         <span id="p_lt_CMSWebPartZone5_pageplaceholder_p_lt_CMSWebPartZone9_ResourceTag_lblFilter" style="display:none;">Filter</span>
											         <span id="p_lt_CMSWebPartZone5_pageplaceholder_p_lt_CMSWebPartZone9_ResourceTag_chklstFilter" class="ContentRadioButtonList">
											            <ul style="display: none;">
											            <?php if(Session::has('classes')) $sel = Session::get('classes'); else $sel=[];?>
											             @foreach ($classes as $key=>$value)
											             	<strong>{{$value->name}}</strong>
											             	@foreach ($value->subClasses as $subkey=>$subvalue)
											             		<li><input id="class_{{$subvalue->id}}" type="checkbox" {{in_array($subvalue->id, $sel) ? 'checked' : '' }} value="{{$subvalue->id}}" data-title="{{$subvalue->name}}"><label for="class_{{$subvalue->id}}">{{$subvalue->name}}</label></li>
											             	@endforeach
											             @endforeach
											            </ul>
											         </span>
											      </div>
											   </dd>
											</dl>                                
		                                </div>
		                                <div class="col-sm-12" style="margin-top:30px;">
		                                    <div class="form-group">
		                                        <div class="input-icon">
		                                            <i class="fa fa-map-marker" style="margin-top: 16px;"></i>
		                                            <input type="text" class="form-control input-lg input-circle custom-typeahead" id="js-text-office" placeholder="Rome, Italy"  value="{{ Session::get('location') }}">
		                                        </div>
		                                    </div>
		                                </div>
		                                <div class="col-sm-4 col-xs-offset-4 text-center" style="margin-top:30px;">
				                            <div class="form-group">
				                                <button type="button" class="btn green btn-block btn-circle btn-lg" id="js-prof-search">
				                                    <i class="fa fa-search"></i>
				                                </button>
				                            </div>
				                        </div>
									</div>									
								</div>
								<div class="tab-pane page-quick-sidebar-alerts" id="quick_sidebar_tab_2">
									<div class="page-quick-sidebar-alerts-list">
										<h3 class="list-heading">Search Servicies</h3>
										<div class="col-sm-12" style="margin-top:30px;">
		                                    <div class="form-group">
		                                        <div class="input-icon">
		                                            <i class="fa fa-pencil" style="margin-top: 16px;"></i>
		                                            <input type="text" class="form-control input-lg input-circle custom-typeahead" id="js-text-keyword" placeholder="Enter Keyword" value="{{ Session::get('keyword') }}">
		                                        </div>
		                                    </div>              
		                                </div>
		                                <div class="col-sm-12" style="margin-top:30px;">
		                                    <div class="form-group">
		                                        <div class="input-icon">
		                                            <i class="fa fa-map-marker" style="margin-top: 16px;"></i>
		                                            <input type="text" class="form-control input-lg input-circle custom-typeahead" id="js-text-location" placeholder="Rome, Italy"  value="{{ Session::get('location') }}">
		                                        </div>
		                                    </div>
		                                </div>
		                                <div class="col-sm-4 text-center col-xs-offset-4" style="margin-top:30px;">
				                            <div class="form-group">
				                                <button type="button" class="btn green btn-block btn-circle btn-lg" id="js-btn-search">
				                                    <i class="fa fa-search"></i>
				                                </button>
				                            </div>
				                        </div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
            <!-- / Sidebar Toggler -->
            
                      
            @show

            @section('main')
            
            @show

            @section('footer')
            <div class="pre-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4 pre-footer-col">
                          <h2>Company Info</h2>
                          <ul>
                            <li><a href="http://finternet-group.com/about-us/" target="_blank">About Us</a></li>
                            <li><a href="http://finternet-group.com/blog-3/" target="_blank">Blog</a></li>
                            <li><a href="http://finternet-group.com/jobs/" target="_blank">Careers</a></li>
                            <li><a href="http://finternet-group.com/media-companies/" target="_blank">Media Companies</a></li>
                            <li><a href="http://finternet-group.com/investors/" target="_blank">Investors</a></li>
                            <li><a href="http://finternet-group.com/contact-us/" target="_blank">Contact &amp; Support</a></li>
                          </ul>
                        </div>
                      
                        <div class="col-sm-4 pre-footer-col">
                          <h2>Our Contacts</h2>
                          <address class="margin-bottom-40">
                          Finternet Group Oy<br>
                          Malmin kauppatie 8b<br>
                          00700 Rome, Italy<br>
                          Website: <a href="http://www.finternet-group.com" target="_blank">www.finternet-group.com</a><br>
                          Phone: +358 45 262 5977<br>
                          Email: mikael@finternet-group.com<br>
                          </address>
                        </div>
                        <div class="col-sm-4 pre-footer-col">
                            <div class="pre-footer-subscribe-box pre-footer-subscribe-box-vertical">
                                <h2>Newsletter</h2>
                                <p>Subscribe to our newsletter and stay up to date with the latest news and deals!</p>
                                <form action="#">
                                <div class="input-group">
                                    <input type="text" placeholder="Enter your email..." class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="submit">Subscribe</button>
                                    </span>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 padding-top-10">
                        2015 &copy; Finternet-Group. ALL Rights Reserved.
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <ul class="social-footer list-unstyled list-inline pull-right">
                                <li><a href="https://www.facebook.com/FinternetGroup" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/9221291?trk=tyah&trkInfo=idx%3A1-1-1%2CtarId%3A1424145037259%2Ctas%3Afinternet" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="https://twitter.com/finternetgroup" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            </ul>  
                        </div>
                    </div>
                </div>
            </div>            
            <form method="get" action="{{ URL::route('store.search') }}" id="js-frm-search">
                <input type="hidden" name="keyword"/>
                <input type="hidden" name="location"/>
                <input type="hidden" name="lat"/>
                <input type="hidden" name="lng"/>
                <input type="hidden" name="dt"/>
            </form>
            <form method="get" action="{{URL::route('store.profsearch')}}" id="prof-search-frm">
	            <input type="hidden" name="classes"/>
	            <input type="hidden" name="location"/>
	            <input type="hidden" name="dt"/>
	            <input type="hidden" name="lat"/>
                <input type="hidden" name="lng"/>
            </form>
            @show
        </body>
    @stop

    @section('page-scripts')
        {{ HTML::script('/assets/metronic/frontend/layout/scripts/back-to-top.js') }}
        {{ HTML::script('/assets/metronic/admin/layout/scripts/layout.js') }}        
        {{ HTML::script('/assets/metronic/admin/pages/scripts/custom.js') }}
        {{ HTML::script('/assets/metronic/global/plugins/typeahead/handlebars.min.js') }}
        {{ HTML::script('/assets/metronic/global/plugins/typeahead/typeahead.bundle.min.js') }}
        {{ HTML::script('//maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true') }}
        {{ HTML::script('/assets/js/brain-socket.min.js') }}
        <script>
        jQuery(document).ready(function(){
        	$('.scroller').slimScroll({
                color:'#57b5e3',        	
    			height:'275px'
            });
        	var html ='', title;
        	$('.top-menu .dropdown-quick-sidebar-toggler a, .page-quick-sidebar-toggler').click(function (e) {
                $('body').toggleClass('page-quick-sidebar-open'); 
            });
        	$(".multiSelect input[type=checkbox]").each(function(idx, elem) {            	      		   
 						if($(this).is(':checked')){ 							
 					        title = $(this).attr("data-title").split(':').pop() + ",";
 							html += '<span title="' + title + '">' + title + '</span>';
 							$(".hida").hide();
 						}
 						$('.multiSel').html(html);
      		});
        });
        var substringMatcher = function(strs) {
            return function findMatches(q, cb) {
                var matches, substrRegex;
                matches = [];
                substrRegex = new RegExp(q, 'i');
                $.each(strs, function(i, str) {
                    if (substrRegex.test(str)) {
                        matches.push({ value: str });
                    }
                });
                cb(matches);
            };
        };
           
        var officies = [];
        @foreach ($officies as $key => $value)
            officies[{{ $key }}] = "{{ $value->address }}";
        @endforeach

        var categories = [];
        @foreach ($categories as $key => $value)
            categories[categories.length] = '{{ $value->name }}';
            @foreach ($value->subCategories as $subKey => $subValue)
                categories[categories.length] = '{{ $subValue->name }}';
            @endforeach
        @endforeach            
           
        $('#js-text-keyword').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'keywords',
            displayKey: 'value',
            source: substringMatcher(categories)
        });
            
        $('#js-text-location').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'officies',
            displayKey: 'value',
            source: substringMatcher(officies)
        });
        $('#js-text-office').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'officies',
            displayKey: 'value',
            source: substringMatcher(officies)
        });
        
        $("#js-text-keyword, #js-text-location").keyup(function(event) {
            if (event.keyCode == 13) {
                $("button#js-btn-search").click();
            }
        });
        
        $("button#js-btn-search").click(function() {
            var classes = '';
        	$("input[name='keyword']").val($("#js-text-keyword").val());
            $("input[name='location']").val($("#js-text-location").val());
            $("#js-frm-search").submit();
        });

		$('button#js-prof-search').click(function(){
			var classes = '';
			$(".multiSelect input[type=checkbox]").each(function(idx, elem) {
     		   var is_checked = $(this).prop("checked");
						if(is_checked == true){
							classes += $(this).val()+',';
						}
     		});
 			classes = classes.slice(0,-1);
 			$("input[name='classes']").val(classes);
 			$("input[name='location']").val($("#js-text-office").val());
 			$("#prof-search-frm").submit();
		});
        
        $('[data-toggle="tooltip"]').tooltip();
        $('input#js-number-rating').rating();
        
        $(document).ready(function() {
            if(navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    $("input[name='lat']").val(position.coords.latitude);
                    $("input[name='lng']").val(position.coords.longitude);
                });
            }
            if($('.messenger').hasClass('open')){
            	$('.messenger').animate({height:"40px"}, 200);
        	    $('.messenger').removeClass("open");console.log('close');
        	    $('.messenger').addClass("close_cb");
        	    $('.chat-footer').hide();
        	    $('#chat-toggle span').removeClass('glyphicon-chevron-down');
        	    $('#chat-toggle span').addClass('glyphicon-chevron-up');
            }
        });

      //live chat
        jQuery(function(){
            var prof_id = '';
 		@if(Session::has('user_id'))
        // var fake_user_id = Math.floor((Math.random()*1000)+1);
        var fake_user_id = {{ Session::get('user_id') }};
        @else 
            var fake_user_id = '';
        @endif

        @if(isset($prof_id))
            prof_id = {{$prof_id}};
        @endif
        
        //make sure to update the port number if your ws server is running on a different one.
        window.app = {};
 
        app.BrainSocket = new BrainSocket(
                new WebSocket('ws://192.168.0.96:8080'),
                new BrainSocketPubSub()
        );
 
        app.BrainSocket.Event.listen('generic.event',function(msg){
            console.log(msg);            
            if(msg.client.data.user_id == fake_user_id){
                $('#chat-log').append('<li><img src="{{ HTTP_USER_PATH. DEFAULT_PHOTO }}" class="img-circle" width="40"><div class="message">'+msg.client.data.message+'</div></li>');
            }else if(msg.client.data.receiver_id == fake_user_id){
                var str_test='<li class="right"><img src="'+msg.client.data.user_portrait+'" class="img-circle" width="40"><div class="message">'+msg.client.data.message+'</div></li>';
                $('#chat-log').append(str_test);
                if ($('.messenger').hasClass("close_cb"))
                	$('#chat-toggle').click();
            }
        });
 
        app.BrainSocket.Event.listen('app.success',function(data){
            console.log('An app success message was sent from the ws server!');
            console.log(data);
        });
 
        app.BrainSocket.Event.listen('app.error',function(data){
            console.log('An app error message was sent from the ws server!');
            console.log(data);
        });
        
        $('#chat-message').keypress(function(event) {
 
            if(event.keyCode == 13){
 
                app.BrainSocket.message('generic.event',
                        {
                            'message':$(this).val(),
                            'user_id':fake_user_id,
                            'user_portrait':'{{HTTP_USER_PATH. DEFAULT_PHOTO}}',
                            'receiver_id': prof_id
                        }
                );
				//save chat history in database by ajax
				var message = $(this).val();
				 $.ajax({
		            url: "{{ URL::route('async.user.savechat') }}",
		            dataType : "json",
		            type : "POST",
		            data : { 
		            	from : fake_user_id, 
		            	to : prof_id, 
		            	message : message,
		            	direction : 'up'
		            	},
		            success : function(data){
		            	
		            }
		            	
		        });
				// \ end ajax
				 $(this).val('');
            } 
            return event.keyCode != 13; }
        );
        
    });

        function scrollChat(){
        	  var s = $('.messenger-body').scrollTop();
        	  var h = $('.messenger-body').height();
        	  //alert( "scrollTop: " + s + " " + "height: " + h)
        	  $('.messenger-body').scrollTop(h);
        	}
        	// jQuery Animation
        	$('#chat-toggle, .open-close-chat').click( function(event){
        	  event.preventDefault();
        	  if ($('.messenger').hasClass("close_cb")){ //open chat box
        	    $('.messenger').animate({height:"395px"}, 200);console.log('open');
        	    $('.messenger').removeClass("close_cb");
        	    $('.messenger').addClass("open");
        	    $('.chat-footer').fadeIn();
        	    $('#chat-toggle span').removeClass('glyphicon-chevron-up');
        	    $('#chat-toggle span').addClass('glyphicon-chevron-down');
        	    scrollChat();
        	  } else{ // close chat box
        	    $('.messenger').animate({height:"40px"}, 200);
        	    $('.messenger').removeClass("open");console.log('close');
        	    $('.messenger').addClass("close_cb");
        	    $('.chat-footer').hide();
        	    $('#chat-toggle span').removeClass('glyphicon-chevron-down');
        	    $('#chat-toggle span').addClass('glyphicon-chevron-up');
        	  }
        	  return false;
        	});
        	// \live chat
        </script>      
    @stop
@stop
