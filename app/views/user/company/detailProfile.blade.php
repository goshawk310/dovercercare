@extends('user.layout')

@section('custom-styles')
{{ HTML::style('/assets/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css') }}
<style>
    #owl-demo .item{
      margin: 7px;
    }
</style>
@stop

@section('main')
    <div class="container">   
    	<div class="row margin-bottom-30 padding-top-normal">
                
                <div class="col-md-3 front-carousel">
                  <div style="position: relative;" class="thumbnail">
                    <?php 
                        $tooltip = "<h4>".$company->name."</h4>";
                        $tooltip.= "<p><b>Keywords : </b>";
                        $keywords = explode(",", $company->keyword);
                        foreach ($keywords as $subKey => $value) {
                            if ($subKey != 0)
                                $tooltip.=", ";
                            $tooltip.= $value;
                        }
                        $tooltip.= "</p>";
                        $tooltip.= "<p><b>Email : </b>".$company->email."</p>";
                        $tooltip.= "<p><b>Phone : </b>".$company->phone."</p>";                        
                        $tooltip.= "<p><b>VAT ID : </b>".$company->vat_id."</p>";
                        $tooltip.= "<p><b>Zip Code : </b>".$company->zip_code."</p>";
                        $tooltip.= "<p><b>Address : </b>".$company->address."</p>";
                        $tooltip.= "<p><b>Description : </b>".substr($company->description, 0, 300)."</p>";
                    ?>
                    <a href="{{ URL::route('company.detail', $company->slug) }}">
                        <div style="height: 250px; width: 100%; background: url({{ HTTP_COMPANY_PATH.$company->photo }}); background-size: cover;" data-toggle="tooltip" data-html="true" data-title="{{ $tooltip }}"></div>
                    </a>
                    <div class="featured-company-offer">
                    @if (count($company->offers) > 0)
                        @foreach ($company->offers as $offer)
                            @if (!$offer->is_review)
                            <div class="color-default store-offers"><b>{{ $offer->name." : " }}</b>{{ $offer->price."&euro;" }}</div>
                            @endif
                        @endforeach                    
                    @endif
                    </div>
                    <h4 class="color-default margin-top-xs">
                        <a href="{{ URL::route('company.detail', $company->slug) }}" data-toggle="tooltip"  data-html="true" data-title="{{ $tooltip }}">
                            {{ $company->name }}
                        </a>
                    </h4>                    
                    <!-- <div class="pull-left">
                        <a href="{{ URL::route('company.detail', $company->slug) }}">
                            Detail&nbsp;<i class="fa fa-angle-double-right"></i>
                        </a>
                    </div> -->
                    <div class="pull-right">                            
                        <input id="js-number-rating" type="number" class="rating" min=0 max=5 step=1 data-show-clear=false data-show-caption=false data-size='xs' value="{{ $company->getRatingScore() }}" readonly=true>
                    </div>
                    <div class="clearfix"></div>
                    
                	</div>
                </div>
                <div class="col-md-6">
                  <h2>About me</h2>
                  <p>{{$company->description}}</p>                  
                </div>
               
               
               <div class="col-md-3 testimonials-v1">
               	<div class="portlet yellow box" style="border:none;background-color:transparent;">
               		<div class="portlet-title" style="background:#E6400C;border-bottom:none;">
               			<div class="caption">Featured Professional</div>
               		</div>
               			<div class="portlet-body" style="display:inline-block;border:1px solid #ddd;border-top:none;">
               				<div class="prof_scroller" style="height: 490px; margin-right:12px;" data-always-visible="1" data-rail-visible="1">
			               		@foreach ($featured_companies as $key => $c)
			               			<div class="col-md-12 carousel-info note note-info" style="padding-right:0px;">
			               				<a href="{{ URL::route('company.detail', $c->slug) }}" style="color:#ffffff;" data-placement="{{ $key % 4 == 3 ? 'left' : 'right' }}" data-html="true">
										     <img class="" src="{{ HTTP_COMPANY_PATH.$c->photo }}" alt="">							                    
										 </a>
										 <a href="{{ URL::route('company.detail', $c->slug) }}" style="color:#333;font-size:14px;font-weight:bold;" data-placement="{{ $key % 4 == 3 ? 'left' : 'right' }}" data-html="true">
								                 {{ $c->name }}
								         </a>
										 <input id="js-number-rating" type="number" class="rating" min=0 max=5 step=1 data-show-clear=false data-show-caption=false data-size='xs' value="{{ $c->getRatingScore() }}" readonly=true>
			               			</div>               			
			               		@endforeach
			               		</div>
			               </div>               		
               		</div>
               </div>
        <div class="row padding-top-normal padding-bottom-normal">
            <div class="col-sm-12 text-center padding-top-normal padding-bottom-normal"><h2 class="color-default">Featured Services</h2></div>        
        </div>
        <div class="row">
            @foreach ($stores as $key => $store)
            <div class="col-sm-3">
                <div style="position: relative;" class="thumbnail">
                    <?php
                        $tooltip = "<h4>".$store->name."</h4>";
                        $tooltip.= "<p><b>Keywords : </b>";
                        $keywords = explode(",", $store->keyword);
                        foreach ($keywords as $subKey => $value) {
                            if ($subKey != 0)
                                $tooltip.=", ";
                            $tooltip.= $value;
                        }
                        $tooltip.= "</p>";
                        $tooltip.= "<p><b>Email : </b>".$store->company->email."</p>";
                        $tooltip.= "<p><b>Phone : </b>".$store->company->phone."</p>";
                        $tooltip.= "<p><b>VAT ID : </b>".$store->company->vat_id."</p>";                        
                        $tooltip.= "<p><b>Description : </b>".substr($store->description, 0, 300)."</p>";
                    ?>
                    <a href="{{ URL::route('store.detail', $store->slug) }}">
                        <div style="height: 145px; width: 100%; background: url({{ HTTP_STORE_PATH.$store->photo }}); background-size: cover;" data-toggle="tooltip" data-placement="{{ $key % 4 == 3 ? 'left' : 'right' }}" data-html="true" data-title="{{ $tooltip }}"></div>
                    </a>
                    <div class="featured-company-offer">
                    @if (count($store->company->offers) > 0)
                        @foreach ($store->company->offers as $offer)
                            @if (!$offer->is_review)
                            <div class="color-default store-offers"><b>{{ $offer->name." : " }}</b>{{ $offer->price."&euro;" }}</div>
                            @endif
                        @endforeach                    
                    @endif
                    </div>
                    <h4 class="color-default margin-top-xs">
                        <a href="{{ URL::route('store.detail', $store->slug) }}" data-toggle="tooltip" data-placement="{{ $key % 4 == 3 ? 'left' : 'right' }}" data-html="true" data-title="{{ $tooltip }}">
                            {{ $store->name }}
                        </a>
                    </h4>                    
                    <div class="pull-left">
                        <a href="{{ URL::route('store.detail', $store->slug) }}">
                            Detail&nbsp;<i class="fa fa-angle-double-right"></i>
                        </a>
                    </div>
                    <div class="pull-right">                            
                        <input id="js-number-rating" type="number" class="rating" min=0 max=5 step=1 data-show-clear=false data-show-caption=false data-size='xs' value="{{ $store->getRatingScore() }}" readonly=true>
                    </div>
                    <div class="clearfix"></div>
                    
                </div>
            </div>
            @endforeach
        </div>
        <hr/>
        <div class="row padding-top-normal padding-bottom-normal">
            <div class="col-sm-12 text-center"><h2 class="color-default">Recent Reviews</h2></div>        
        </div>
        <div class="row padding-bottom-normal">
            <div class="col-sm-12">
                <div id="owl-demo" class="owl-carousel owl-theme">
                    @foreach ($feedbacks as $feedback)
                    <div class="item">
                        <div class="testimonials-v1">
                            <blockquote><p>{{ $feedback->description }}</p></blockquote>
                            <table>
                                @foreach ($feedback->ratings as $rating)
                                <tr>
                                    <td class="text-right"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $rating->type->name }}&nbsp;:&nbsp;</b></td>
                                    <td>
                                        <input id="js-number-rating" type="number" class="rating" min=0 max=5 step=1 data-show-clear=false data-show-caption=false data-size='xs' value="{{ $rating->answer }}" readonly=true>                                
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                            <div class="carousel-info">
                                <img class="pull-left" src="{{ HTTP_USER_PATH.$feedback->user->photo }}" style="height: 60px; width: 60px;">
                                <div class="pull-left">
                                    <span class="testimonials-name" style="margin-top: 10px;">{{ $feedback->user->name }}</span>
                                    <span class="testimonials-post"><i class="fa fa-clock-o"></i>&nbsp;{{ date(DATE_FORMAT, strtotime($feedback->created_at)) }}</span>
                                </div>
                            </div>                    
                        </div>
                    </div>
                    @endforeach
                </div>    
            </div>        
        </div>
      </div>
    </div>
        
    
@stop

@section('custom-scripts')
    {{ HTML::script('/assets/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js') }}    
    {{ HTML::script('/assets/js/bootstrap-tooltip.js') }}
    @include('js.user.home.index')
    <script>
    $(document).ready(function() {
        $("#owl-demo").owlCarousel({            
            autoPlay: 3000, //Set AutoPlay to 3 seconds       
            items : 4,
            itemsDesktop : [1199,3],
            itemsDesktopSmall : [979,3]
        });
        $('.prof_scroller').slimScroll({
            color:'#57b5e3',        	
			height:'480px'
        });
    });    
    </script>      
@stop

@stop
