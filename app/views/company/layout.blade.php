@extends('main')
    @section('page-styles')
        {{ HTML::style('/assets/metronic/admin/layout/css/layout.css') }}
        {{ HTML::style('/assets/metronic/admin/layout/css/themes/default.css') }}
        {{ HTML::style('/assets/metronic/admin/layout/css/custom.css') }}
        {{ HTML::style('/assets/css/style_company.css') }}
        {{ HTML::style('/assets/css/messenger.css') }}
    @stop

    @section('body')
        <body class="page-header-fixed page-quick-sidebar-over-content">
            @section('header')
     <!--  live chat -->
     @if(Session::has('company_id'))
			<div class="messenger bg-white open">
			    <div class="chat-header text-white bg-gray-dark">
			    	<div class="titlebar clearfix">
			    		<div class="top-lfloat">
			    			<div class="usr-status isOnline"></div>
			    			<div class="usr-name">
			    				<strong>{{Session::get('company_name')}}</strong>
			    			</div>
			    		</div>
			    		<div class="open-chat-target open-close-chat"></div>
			    	</div>
			        
			        <a href="#" id="chat-toggle" class="pull-right chat-toggle">
			            <span class="glyphicon glyphicon-chevron-down"></span>
			        </a>
			    </div>
			    <div class="messenger-body" >
			        <ul class="chat-messages" id="chat-log">
			 
			        </ul>
			        <div class="chat-footer">
			            <div class="p-lr-10">
			                <textarea id="chat-message"
			                    class="input-light brad chat-search" placeholder="Your message..."></textarea>
			            </div>
			        </div>
			    </div>
			</div>
		@endif
	<!-- / End live chat -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="{{ URL::route('user.home') }}">
                            <img src="/assets/img/logo_company.png" alt="logo" class="logo-default" style="height: 38px; margin-top: 3px;">
                        </a>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->

                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">                    
                        <ul class="nav navbar-nav pull-right">
                            @if (Session::has('company_id'))
                            <li class="dropdown dropdown-quick-sidebar-toggler">
    					        <a href="#" class="dropdown-toggle">
					                {{ Session::get('company_name') }}
    					        </a>
    				        </li>
    				        <li class="dropdown dropdown-quick-sidebar-toggler">
    					        <span class="badge badge-info" style="margin-top:18px;">
									@if(Session::get('company_type') == 'basic')
					                	Basic Member
					                @else
					                	Pro Member
					                @endif
								</span>
    					        	
    					        
    				        </li>
                            <li class="dropdown dropdown-quick-sidebar-toggler">
                                <a href="{{ URL::route('company.auth.logout') }}" class="dropdown-toggle">
                                    <i class="icon-logout"></i> Sign Out
                                </a>
                            </li>
                            @else
                            <li class="dropdown dropdown-quick-sidebar-toggler {{ ($pageNo == 51) ? 'active' : '' }}">
                                <a href="{{ URL::route('company.auth.login') }}" class="dropdown-toggle">
                                    <i class="icon-login"></i>
                                    Sign In
                                </a>
                            </li>
                            <li class="dropdown dropdown-quick-sidebar-toggler {{ ($pageNo == 52) ? 'active' : '' }}">
                                <a href="{{ URL::route('company.auth.membership') }}" class="dropdown-toggle">
                                    <i class="icon-note"></i>
                                    Register
                                </a>
                            </li>
                            @endif
                        </ul>                        
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <div class="clearfix"></div>
            @show

            @section('main')
            <?php if (!isset($pageNo)) { $pageNo = 0; } ?>
            <div class="page-container">
                <div class="page-sidebar-wrapper">
                    <div class="page-sidebar navbar-collapse collapse">
                        <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
                            <li class="sidebar-toggler-wrapper">
                                <div class="sidebar-toggler"></div>
                            </li>
                            
                            <li class="start {{ ($pageNo == 1) ? 'active' : '' }}">
                                <a href="{{ URL::route('company.dashboard') }}">
                                    <i class="icon-bar-chart"></i>
                                    <span class="title">Dashboard</span>
                                </a>
                            </li>
                            
                            <li class="{{ ($pageNo == 13) ? 'active' : '' }}">
                                <a href="{{ URL::route('company.store') }}">
                                    <i class="fa fa-bank"></i>
                                    <span class="title">Service Management</span>
                                </a>
                            </li>                            
                            <li class="{{ ($pageNo == 3) ? 'active' : '' }}">
                                <a href="{{ URL::route('company.book') }}">
                                    <i class="fa fa-shopping-cart"></i>
                                    <span class="title">Book Management</span>
                                </a>
                            </li>
                            <li class="{{ ($pageNo == 2) ? 'active' : '' }}">
                                <a href="{{ URL::route('company.user') }}">
                                    <i class="icon-users"></i>
                                    <span class="title">Consumer Management</span>
                                </a>
                            </li>

                            
                            <!-- li class="{{ ($pageNo == 4) ? 'active' : '' }}">
                                <a href="{{ URL::route('company.comment') }}">
                                    <i class="fa fa-comments"></i>
                                    <span class="title">Comment Management</span>
                                </a>
                            </li -->
                            <li class="{{ ($pageNo == 11) ? 'active' : '' }}">
                                <a href="{{ URL::route('company.ratingType') }}">
                                    <i class="fa fa-bookmark"></i>
                                    <span class="title">Rating Type Management</span>
                                </a>
                            </li>
                            <!-- li class="{{ ($pageNo == 5) ? 'active' : '' }}">
                                <a href="#">
                                    <i class="fa fa-star"></i>
                                    <span class="title">Rating Management</span>
                                </a>
                            </li -->
                            <li class="{{ ($pageNo == 6) ? 'active' : '' }}">
                                <a href="{{ URL::route('company.feedback') }}">
                                    <i class="fa fa-edit"></i>
                                    <span class="title">Feedback Management</span>
                                </a>
                            </li>
                            
                            <li class="{{ ($pageNo == 14) ? 'active' : '' }}">
                                <a href="{{ URL::route('company.message') }}">
                                    <i class="fa fa-comment"></i>
                                    <span class="title">Message Management</span>
                                </a>
                            </li>                            
                            
                            <li class="{{ ($pageNo == 7) ? 'active' : '' }}">
                                <a href="{{ URL::route('company.offer') }}">
                                    <i class="fa fa-gavel"></i>
                                    <span class="title">Offer Management</span>
                                </a>
                            </li>
                            
                            <li class="{{ ($pageNo == 8) ? 'active' : '' }}">
                                <a href="{{ URL::route('company.loyalty') }}">
                                    <i class="fa fa-heart"></i>
                                    <span class="title">Loyalty Management</span>
                                </a>
                            </li>
                            
                            <li class="{{ ($pageNo == 5) ? 'active' : '' }}">
                                <a href="{{ URL::route('company.office') }}">
                                    <i class="fa fa-map-marker"></i>
                                    <span class="title">Office Management</span>
                                </a>
                            </li>
                            @if(Session::get('company_type') != 'pro_py')
                            <li class="{{ ($pageNo == 12) ? 'active' : '' }}">
                                <a href="{{ URL::route('company.subscribe') }}">
                                    <i class="fa fa-trophy"></i>
                                    <span class="title">Subscribe Management</span>
                                </a>
                            </li>                            
                            @endif
                            <li class="{{ ($pageNo == 10) ? 'active' : '' }}">
                                <a href="{{ URL::route('company.widget.index') }}">
                                    <i class="fa fa-gear"></i>
                                    <span class="title">Widgets</span>
                                </a>
                            </li>
                            
                            <li class="last {{ ($pageNo == 9) ? 'active' : '' }}">
                                <a href="{{ URL::route('company.profile') }}">
                                    <i class="fa fa-building"></i>
                                    <span class="title">Professional Profile</span>
                                </a>
                            </li>
                            
                            <li class="{{ ($pageNo == 15) ? 'active' : '' }}">
                                <a href="{{ URL::route('company.contact') }}">
                                    <i class="fa fa-envelope"></i>
                                    <span class="title">Contact Us</span>
                                </a>
                            </li>                            
                        </ul>
                    </div>
                </div>
                <div class="page-content-wrapper">
                    <div class="page-content">
                        @yield('breadcrumb')
                        @yield('content')
                    </div>
                </div>
            </div>            
            @show

            @section('footer')
                <div class="page-footer footer-background">
                    <div class="page-footer-inner">
                         &copy; Copyright 2015 | All Rights Reserved | Powered by Finternet-Group
                    </div>
                    <div class="page-footer-tools">
                        <span class="go-top">
                        <i class="fa fa-angle-up"></i>
                        </span>
                    </div>
                </div>
            @show
        </body>
    @stop

    @section('page-scripts')        
        {{ HTML::script('/assets/metronic/admin/layout/scripts/layout.js') }}
        {{ HTML::script('/assets/metronic/admin/layout/scripts/quick-sidebar.js') }}
        {{ HTML::script('/assets/metronic/admin/layout/scripts/layout.js') }}
        {{ HTML::script('/assets/metronic/admin/pages/scripts/components-form-tools.js') }}
        {{ HTML::script('/assets/js/brain-socket.min.js') }}
        <script>
        jQuery(document).ready(function() {
            Metronic.init(); // init metronic core components
            Layout.init(); // init current layout
            QuickSidebar.init() // init quick sidebar

            $("a#js-a-delete").click(function(event) {
                event.preventDefault();
                var url = $(this).attr('href');
                bootbox.confirm("Are you sure?", function(result) {
                    if (result) {
                        window.location.href = url;
                    }
                });
            });

            $('[data-toggle="tooltip"]').tooltip();

            if($('.messenger').hasClass('open')){
            	$('.messenger').animate({height:"40px"}, 200);
        	    $('.messenger').removeClass("open");console.log('close');
        	    $('.messenger').addClass("close_cb");
        	    $('.chat-footer').hide();
        	    $('#chat-toggle span').removeClass('glyphicon-chevron-down');
        	    $('#chat-toggle span').addClass('glyphicon-chevron-up');
            }
        });

      //live chat
      
        jQuery(function(){
            var receiver_id = 1;
        @if(Session::has('company_id'))
        var fake_user_id = {{ Session::get('company_id') }};
        @else
            var fake_user_id = ''
        @endif
        //make sure to update the port number if your ws server is running on a different one.
        window.app = {};
 
        app.BrainSocket = new BrainSocket(
                new WebSocket('ws://192.168.0.96:8080'),
                new BrainSocketPubSub()
        );
 
        app.BrainSocket.Event.listen('generic.event',function(msg){
            console.log(msg);
             receiver_id = msg.client.data.user_id;
            if(msg.client.data.user_id == fake_user_id){
                $('#chat-log').append('<li><img src="{{ HTTP_USER_PATH. DEFAULT_PHOTO }}" class="img-circle" width="40"><div class="message">'+msg.client.data.message+'</div></li>');
            }else if(msg.client.data.receiver_id == fake_user_id){
                var str_test='<li class="right"><img src="'+msg.client.data.user_portrait+'" class="img-circle" width="40"><div class="message">'+msg.client.data.message+'</div></li>';
                $('#chat-log').append(str_test);
                if ($('.messenger').hasClass("close_cb"))
                	$('#chat-toggle').eq(0).click();
            }
        });
 
        app.BrainSocket.Event.listen('app.success',function(data){
            console.log('An app success message was sent from the ws server!');
            console.log(data);
        });
 
        app.BrainSocket.Event.listen('app.error',function(data){
            console.log('An app error message was sent from the ws server!');
            console.log(data);
        });
 
        $('#chat-message').keypress(function(event) {
 
            if(event.keyCode == 13){
 console.log(fake_user_id);
                app.BrainSocket.message('generic.event',
                        {
                            'message':$(this).val(),
                            'user_id':fake_user_id,
                            'user_portrait':'{{HTTP_USER_PATH. DEFAULT_PHOTO}}',
                            'receiver_id': receiver_id
                        }
                );
                var message = $(this).val();
				 $.ajax({
		            url: "{{ URL::route('async.user.savechat') }}",
		            dataType : "json",
		            type : "POST",
		            data : { 
		            	from : fake_user_id, 
		            	to : receiver_id, 
		            	message : message,
		            	direction : 'pu'
		            	},
		            success : function(data){
		            	
		            }		            	
		        }); 
				 $(this).val('');
            }
 
            return event.keyCode != 13; }
        );
    });

        function scrollChat(){
        	  var s = $('.messenger-body').scrollTop();
        	  var h = $('.messenger-body').height();
        	  //alert( "scrollTop: " + s + " " + "height: " + h)
        	  $('.messenger-body').scrollTop(h);
        	}
        	// jQuery Animation
        	$('#chat-toggle, .open-close-chat').click( function(event){
        	  event.stopPropagation();
        	  if ($('.messenger').hasClass("close_cb")){ //open chat box
        	    $('.messenger').animate({height:"395px"}, 200);console.log('open');
        	    $('.messenger').removeClass("close_cb");
        	    $('.messenger').addClass("open");
        	    $('.chat-footer').fadeIn();
        	    $('#chat-toggle span').removeClass('glyphicon-chevron-up');
        	    $('#chat-toggle span').addClass('glyphicon-chevron-down');
        	    scrollChat();
        	  } else{ // close chat box
        	    $('.messenger').animate({height:"40px"}, 200);
        	    $('.messenger').removeClass("open");console.log('close');
        	    $('.messenger').addClass("close_cb");
        	    $('.chat-footer').hide();
        	    $('#chat-toggle span').removeClass('glyphicon-chevron-down');
        	    $('#chat-toggle span').addClass('glyphicon-chevron-up');
        	  }
        	  return false;
        	});
        	
        </script>        
    @stop
@stop
    