@extends('company.layout')

@section('custom-styles')]
    <style>
      #map-canvas {
        height: 300px;
      }
    </style>
    {{ HTML::style('/assets/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}
    {{ HTML::style('/assets/metronic/global/plugins/jquery-tags-input/jquery.tagsinput.css') }}
@stop

@section('breadcrumb')
	<div class="row">
		<div class="col-md-12">
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<span>Service</span>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<span>Create</span>
				</li>
			</ul>
			
		</div>
	</div>    
@stop

@section('content')

@if ($errors->has())
<div class="alert alert-danger alert-dismissibl fade in">
    <button type="button" class="close" data-dismiss="alert">
        <span aria-hidden="true">&times;</span>
        <span class="sr-only">Close</span>
    </button>
    @foreach ($errors->all() as $error)
		{{ $error }}		
	@endforeach
</div>
@endif

<div class="portlet box red">
    <div class="portlet-title">
		<div class="caption">
			<i class="fa fa-pencil-square-o"></i> Create Service
		</div>
	</div>
	<div class="portlet-body form">
        <form class="form-horizontal form-bordered form-row-stripped" role="form" method="post" action="{{ URL::route('company.store.store') }}" enctype="multipart/form-data">
            <input type="hidden" name="company_id" value="{{ $company->id }}"/>
            <div class="form-body">            
                @foreach ([
                    'name' => 'Name',                    
                    'description' => 'Description',
                    'keyword' => 'Keyword',
                    'office_id' => 'Office *',
                    'price' => 'Price *',
                    'photo' => 'Photo',
                    'duration' => 'Duration'
                ] as $key => $value)
                
                <div class="form-group">
                    <label class="col-sm-3 control-label">{{ Form::label($key, $value) }}</label>
                    @if($key == 'price')
                    <div class="col-sm-3">
                    	<?php $prices = ['FP'=>'Full Price','HP'=>'Price/hrs','AI'=>'Ask Info']; ?>
                        	{{Form::select($key,
	                        	$prices,
	                        	NULL,
	                        	array('class'=>'form-control')
                        	)}}
                    </div>
                    <div class="col-sm-3">
                    	<input type="text" name="price_value" class="form-control" placeholder="Price in Euro" required/>
                    </div>
                    @elseif ($key == 'office_id')
                    	<div class="col-sm-7">
                    		<select class="form-control" multiple="multiple" id="{{$key}}" name="{{$key}}[]">
                    			@foreach ($officies as $office)
                    				<option value="{{$office->id}}" data-lat="{{$office->lat}}" data-lng="{{$office->lng}}"  >{{ $office->name }}</option>
                    			@endforeach
                    		</select>
                    	</div>
                        <div class="col-sm-2">
                        	<button type="button" id="addrMarker" class="btn btn-default">
		                        <span class="glyphicon glyphicon-map-marker"></span> Find Address
		                    </button>
                        </div>
                  <!-- Duration Setting imak 20150520 -->
                    @elseif($key == 'duration')
						<div class="col-md-9">
							<input id="tags_2" type="text" placeholder="Price in Euro" class="form-control tags medium" name="{{$key}}" value="1hr,1.5hr,2hr"/>
						</div>
					<!-- / Duration Setting imak 20150520 -->
                           @else
                    <div class="col-sm-9">
                        @if ($key == 'description')
                        <textarea class="form-control" name="{{ $key }}" rows="3"></textarea>
                        @elseif ($key == 'keyword')
                        <textarea class="form-control" name="{{ $key }}" rows="3"></textarea>
                        @elseif ($key == 'photo')
                        <div class="fileinput fileinput-new" data-provides="fileinput">
    						<div class="fileinput-new thumbnail" style="width: 120px; height: 120px;">
    							<img src="{{ HTTP_COMPANY_PATH }}no_image.gif " alt=""/>
    						</div>
    						<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 120px; max-height: 120px;"></div>
    						
    						<div>
    							<span class="btn default btn-file">
    							    <span class="fileinput-new">Select image </span>
    							    <span class="fileinput-exists">Change </span>
    							    <input type="file" name="{{ $key }}">
    							</span>
    							<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput">Remove </a>
    						</div>
						</div>                                              
                        @else
                        <input type="text" class="form-control" name="{{ $key }}" value="">
                        @endif
                        </div>
                        @endif                    
                </div>
                @endforeach
				<div class="form-group">
					<label class="control-label col-md-3">Location</label>
					<div class="col-md-9">
					    <div id="map-canvas"></div>
					</div>
					<input type="hidden" name="lat" value="{{ DEFAULT_LAT }}"/>
					<input type="hidden" name="lng" value="{{ DEFAULT_LNG }}"/>
				</div>
                <div class="form-group" id="js-div-sub-category">
                    <label class="col-sm-3 control-label">Category</label>
                    <div class="col-md-9">
				        <?php
				        $subCategories = [];
				        if(!empty($store->subCategories)){
				        	foreach ($store->subCategories as $item) {
								$subCategories[] = $item->sub_category_id;
							}	
				        }
				         
				        ?>
				        
				        @foreach ($categories as $category)
				            <div class="col-md-4">
				                <p><b>{{ $category->name }}</b></p>
				                @foreach ($category->subCategories as $subCategory)
				                <p>
				                    <input type="checkbox" class="form-control" id="js-checkbox-sub-category" value="{{ $subCategory->id }}" 
				                        {{ in_array($subCategory->id, $subCategories) ? 'checked' : '' }}>&nbsp;{{ $subCategory->name }}
			                    </p>
				                @endforeach
				            </div>
				        @endforeach                    
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="col-sm-12 text-center">
                    <button type="submit"  class="btn btn-success">
                        <span class="glyphicon glyphicon-ok-circle"></span> Save
                    </button>
                    <a href="{{ URL::route('company.store') }}" class="btn btn-primary">
                        <span class="glyphicon glyphicon-share-alt"></span> Back
                    </a>
                </div>
            </div>            
        </form>
    </div>
</div>
@stop

@section('custom-scripts')
{{ HTML::script('/assets/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}
{{ HTML::script('/assets/metronic/global/plugins/jquery-tags-input/jquery.tagsinput.js') }}
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>

<script>
var map;
var marker;
var lat,lng;
var address = new Array();
var myLatlng, mapOptions;
var markers = new Array();
$(document).ready(function() {
	ComponentsFormTools.init();
	lat = $("input[name='lat']").val();
	lng = $("input[name='lng']").val();
	myLatlng = new google.maps.LatLng(lat,lng);
    mapOptions = {
      zoom: 10,
      center:myLatlng      
    }
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: 'Office Location'
    });
    markers.push(marker);

    //choose only 1 category
    $("input[type='checkbox']").change(function(){
    	var len = $('input[type="checkbox"]:checked').length;
    	console.log($(this)[0]);
    	if(len > 1){
    		$(this).attr('checked',false);
    		bootbox.alert('You can choose only 1 category.');
            window.setTimeout(function(){
                bootbox.hideAll();
            }, 2000);
    	}
    });
});




function validate() {
    if ($("select[name='office_id']").val() == '') {
        bootbox.alert("Please select the office");
        return false;
    }
        
    var objList = $("input#js-checkbox-sub-category:checked");
    for (var i = 0; i < objList.length; i++) {
        $("div#js-div-sub-category").append($("<input type='hidden' name='sub_category[]' value=" + objList.eq(i).val() + ">"));
    }
    return true;
}

jQuery('#addrMarker').click(function(){
	if(markers.length > 0){
		for(var i = 0; i < markers.length; i++) {	        
	          markers[i].setMap(null);	        
	  	}
	}	
	
	$('select#office_id :selected').each(function(i, selected){ 
	  address[i] = new Array();
	  address[i]['lat'] = $(selected).attr('data-lat');
	  address[i]['lng'] = $(selected).attr('data-lng');
	  markerAddress(address[i]['lat'],address[i]['lng']);
	});
	AutoCenter();
});


function markerAddress(lat,lng){
	myLatlng = new google.maps.LatLng(lat,lng);
    marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: 'Office Location'
    });
    markers.push(marker);
}

function AutoCenter() {
    //  Create a new viewpoint bound
    var bounds = new google.maps.LatLngBounds();
    //  Go through each...
    $.each(markers, function (index, marker) {
    bounds.extend(marker.position);
    });
    //  Fit these bounds to the map
    map.fitBounds(bounds);
  }
  
</script>

@stop

@stop
