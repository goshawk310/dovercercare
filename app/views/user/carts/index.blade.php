@extends('user.layout')

@section('main')
<div class="container">
    <div class="row">
        <div class="col-sm-7 margin-top-normal margin-bottom-normal">
            @if (count($carts) > 0) 
                <div class="pull-right">{{ $carts->links() }}</div>
                <div class="clearfix"></div>
                @foreach ($carts as $cart)
                <?php $company = $cart->store->company; $store = $cart->store; ?>
                <div class="row margin-top-xs">
                    <div class="col-md-4 col-sm-4">
                        <a href="{{ URL::route('store.detail', $company->slug) }}">
                            <?php 
                            	$tooltip = "<div class='testimonials-v1'><div class='carousel-info'><img style='width:50px;height:50px;float:left;' src='". HTTP_COMPANY_PATH.$company->photo ."'/><h4 style='float:left;margin-left:10px;margin-top:12px;'>".$company->name."</h4></div><div style='clear:both;'></div></div>";
                                //$tooltip .= "<h4>".$company->name."</h4>";
                                $tooltip.= "<p style='margin-top:10px;'><b>Keywords : </b>";
                                $keywords = explode(",", $company->keyword);
                                foreach ($keywords as $key => $value) {
                                    if ($key != 0)
                                        $tooltip.=", ";
                                    $tooltip.= $value;
                                }
                                $tooltip.= "</p>";
                                $tooltip.= "<p><b>Sub Name : </b>".$store->sub_name."</p>";
                                $tooltip.= "<p><b>Email : </b>".$company->email."</p>";
                                $tooltip.= "<p><b>Phone : </b>".$company->phone."</p>";
                               // $tooltip.= "<p><b>Opening : </b>".$store->opening->{strtolower(date('D'))."_start"}." - ".$store->opening->{strtolower(date('D'))."_end"}."</p>";
                                $tooltip.= "<p><b>VAT ID : </b>".$company->vat_id."</p>";                               
                                $tooltip.= "<p><b>Revenue : </b>".$store->revenue."</p>";
                                $tooltip.= "<p><b>Employs : </b>".$store->employs."</p>";
                                $tooltip.= "<p><b>Website : </b>".$store->website."</p>";
                                $tooltip.= "<p><b>Description : </b>".substr($store->description, 0, 300)."</p>";
                            ?>
                            <img class="img-responsive img-rounded" alt="" src="{{ HTTP_STORE_PATH.$store->photo }}" data-toggle="tooltip" data-placement="right" data-html="true" data-title="{{ $tooltip }}">
                        </a>
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <h3 class="">
                            <a href="{{ URL::route('store.detail', $company->slug) }}">{{ $store->name }}</a>
                            <?php if($cart->status == 0) { //pending...
                            		$status_class = "label-info";$status = "Pending";
                            	}elseif($cart->status == 1) { $status_class = "label-success"; $status = "Completed";}// completed...
                            	elseif($cart->status == 2) {$status_class = "label-warning"; $status = "Cancelled";}// cancelled...
                            	else {$status_class = "label-danger"; $status = "Book";}//book
                            ?> 
                            <span class="label label-sm {{ $status_class }}">{{ $status }}</span>
                            <button class="btn btn-link btn-sm" data-id="{{ $cart->id }}" id="js-btn-remove-cart" title="Remove Cart"><i class="fa fa-trash-o"></i></button>
                        </h3>
                        <div class="pull-left">             
                            <input id="js-number-rating" type="number" class="rating" min=0 max=5 step=1 data-show-clear=false data-show-caption=false data-size='xs' value="{{ $store->getRatingScore() }}" readonly=true>          
                        </div>
                        <div class="clearfix"></div>
                        <ul class="blog-info">
                            <li><span class="color-default"><b><i class="fa fa-phone"></i> {{ $company->phone }}</b></span></li>
                            <li><span class="color-default"><b><i class="fa fa-phone"></i> {{ $company->email }}</b></span></li>
                         <!--    <li><i class="fa fa-clock-o"></i> <b>{{ $store->opening->{strtolower(date('D'))."_start"}." - ".$store->opening->{strtolower(date('D'))."_end"} }}</b></li> -->
                        </ul>                        
                        
                        <ul class="blog-info">
                            <li><span class="color-default"><b><i class="fa fa-calendar"></i> Booking Date : </b></span> {{ $cart->book_date }}</li>
                            <li><span class="color-default"><b><i class="fa fa-clock-o"></i> Duration : </b></span> {{ $cart->duration }}</li>                            
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <hr/>
                    </div>
                </div>
                @endforeach
                <div class="pull-right">{{ $carts->links() }}</div>
                <div class="clearfix"></div>
            @else
                <div class="note note-info">
				    <h4 class="block">The cart is empty.</h4>
				</div>
            @endif
        </div>
                
        <div class="col-sm-5">
            <div id="map-canvas" style="height: 500px; width: 100%; border: 2px solid #E6400C;" class="margin-top-sm margin-bottom-xs"></div>
            <a href="http://inquirymall.com" target="_blank" class="margin-top-xs"><img src="/assets/img/partner1.jpg" class="img-responsive"/></a>
            <a href="http://socialheadhunter.org" target="_blank" class="margin-top-xs"><img src="/assets/img/partner2.jpg" class="img-responsive"/></a>
            <a href="http://klikkaaja.com" target="_blank" class="margin-top-xs"><img src="/assets/img/partner3.jpg" class="img-responsive"/></a>
        </div>
    </div>
</div>
@stop

@section('custom-scripts')
{{ HTML::script('/assets/js/bootstrap-tooltip.js') }}
<script>
$(document).ready(function() {
    $("button#js-btn-remove-cart").click(function() {
        var obj = $(this);
        $.ajax({
            url: "{{ URL::route('async.user.cart.remove') }}",
            dataType : "json",
            type : "POST",
            data : { cart_id : $(this).attr('data-id') },
            success : function(data){
                obj.parents("div.margin-top-xs").eq(0).next().remove();
                obj.parents("div.margin-top-xs").eq(0).remove();
                bootbox.alert(data.msg);
                window.setTimeout(function(){
                    bootbox.hideAll();
                }, 2000);
            }
        });        
    });
});
function initialize() {

    var mapPosition = new google.maps.LatLng( {{ DEFAULT_LAT }}, {{ DEFAULT_LNG }});
    var mapOptions = { zoom: 12, center: mapPosition };
    
    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    var marker = [];
    var infowindow = [];
    @foreach ($carts as $key => $cart)
    <?php $value = $cart->store->company; ?>
    var contentString = '' 
    + '<div style="width: 220px;">'
    + '    <p><b>Name : </b>{{ $value->name }}</p>'
    + '    <p><b>Description : </b>{{ addslashes(substr($cart->store->description, 0, 150)) }}</p>'
    + '    <p><i class="fa fa-phone"></i> {{ $cart->store->phone }}&nbsp;&nbsp;&nbsp;'
  //  + '       <i class="fa fa-clock-o"></i> {{ $cart->store->opening->{strtolower(date("D"))."_start"}." - ".$cart->store->opening->{strtolower(date("D"))."_end"} }}</p>'
    + '    <p><b>Address : </b> {{ $cart->store->address }}</p>'
    + '    <p><b>Zip Code : </b> {{ $cart->store->zip_code }}&nbsp;&nbsp;&nbsp;'
    + '       <b>City : </b> {{ isset($cart->store->city_id) ? $cart->store->city->name : "---"; }}</p>'
    + '</div>';
    infowindow[{{ $key }}] = new google.maps.InfoWindow({
        content: contentString
    });    
    
    marker[{{ $key }}] = new google.maps.Marker({position: new google.maps.LatLng({{ $cart->store->lat }}, {{ $cart->store->lng }}), map: map, title: '{{ $value->name }}'});
    
    google.maps.event.addListener(marker[{{ $key }}], 'mouseover', function() {
        infowindow[{{ $key }}].open(map, marker[{{ $key }}]);
    });    
    google.maps.event.addListener(marker[{{ $key }}], 'mouseout', function() {
        infowindow[{{ $key }}].close();
    });    
    @endforeach
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
@stop

@stop
