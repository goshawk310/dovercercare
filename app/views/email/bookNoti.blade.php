<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div>
            <h2>Congratulation!</h2>
        </div>
        <div>
            <p>A customer have booked on your {{ $store_name }} service</p>
            <p>Customer Name : {{ $user_name }}</p>
            <p>Office : {{$location}}</p>
            <p>Address : {{$addr}}</p>            
            <p>Book Date : {{ $book_date }}</p>
            <?php if(!empty($duration)){
            	echo '<p>Duration : {{ $duration }}</p>';
            }?>            
            <p>Price : {{ $price }}</p>
            <p>Message : {{ $msg }}</p>
        </div>
        <hr/>
        <div>
            <b>From {{ SITE_NAME }} Team</b>
        </div>
    </body>
</html>
